const Server = {};
const BASE_URL = "api/";

// Get requests
// const GET_ALL = BASE_URL + "customers";
const GET_BY_CUSTOMER_ID = BASE_URL + "customer/{customerId}";
const GET_LOGGED_IN_CUSTOMER = BASE_URL + "customer";
const GET_MY_CHANNELS = BASE_URL + "channels";
const GET_CHANNEL_MESSAGES = BASE_URL + "channel/messages/{channelId}/{offset}";
const SEARCH_CHANNELS = BASE_URL + "search/channel/{searchText}";
const SEARCH_CUSTOMERS = BASE_URL + "search/customer/{searchText}";

// Post requests
const FORGET_PASSWORD = BASE_URL + "customer/forget_password";
const CHANGE_PASSWORD = BASE_URL + "customer/change_password";
const ADD_NEW_CHANNEL = BASE_URL + "channel";
const ADD_NEW_CHANNEL_MESSAGE = BASE_URL + "channel/message";
const SUBSCRIBE_TO_CHANNEL = BASE_URL + "subscribe";
const UNSUBSCRIBE_TO_CHANNEL = BASE_URL + "unsubscribe";
const LOGIN_URL = BASE_URL + "login";
const LOGOUT_URL = BASE_URL + "logout";
const READ_ALL_MESSAGES = BASE_URL + "channel/readall";

const GET = "GET";
const POST = "POST";

const call = function (method, url, data, callback) {
    console.log("Server", "call |REQUEST|:", method, url, data);
    if (method == GET) {
        $.ajax({
            method: GET,
            url: url,
            success: function (result) {
                console.log("Server", "call |RESPONSE|:", method, url, result);
                callback(result);
            }, error: function (error) {
                var err;
                try {
                    err = JSON.parse(error.responseText);
                } catch (e) {
                    err = error.responseText;
                }
                console.log("Server", "call |ERROR|:", method, url, err);
                callback(null, err);
            }
        });
        return;
    } else if (method == POST) {
        $.ajax({
            method: POST,
            url: url,
            data: JSON.stringify(data),
            success: function (result) {
                console.log("Server", "call |RESPONSE|:", method, url, result);
                callback(result);
            }, error: function (error) {
                var err;
                try {
                    err = JSON.parse(error.responseText);
                } catch (e) {
                    err = error.responseText;
                }
                console.log("Server", "call |ERROR|:", method, url, err);
                callback(null, err);
            }
        });
        return;
    }
    callback(null, {message: "Invalid ajax method"});
};

/**
 * Get current logged in user data from session
 * @param callback
 */
Server.getUserData = function (callback) {
    call(GET, GET_LOGGED_IN_CUSTOMER, {}, callback);
};

/**
 * Get customer data from server side by cusotmer id
 * @param customerId the related customer id
 * @param callback
 */
Server.getCustomerById = function (customerId, callback) {
    var url = GET_BY_CUSTOMER_ID.replace("{customerId}", encodeURIComponent(customerId));
    call(GET, url, {}, callback);
};


/**
 * Get current logged in user channels form server as json object
 * split to two keys {private, public} channels
 * @param callback
 */
Server.getMyChannels = function (callback) {
    call(GET, GET_MY_CHANNELS, {}, callback);
};
/**
 * Search channels by channel name in the server
 * @param text the search text from client
 * @param callback
 */
Server.searchChannels = function (text, callback) {
    var url = SEARCH_CHANNELS.replace("{searchText}", encodeURIComponent(text));
    call(GET, url, {}, callback);
};
/**
 * Search customers to add to private channel by customer nickname in the server
 * @param text the search text from client
 * @param callback
 */
Server.searchCustomers = function (text, callback) {
    var url = SEARCH_CUSTOMERS.replace("{searchText}", encodeURIComponent(text));
    call(GET, url, {}, callback);
};

/**
 * Get specific channel messages by channel id and the last visible message in the client side
 * to support paging for 10 messages per request
 * @param channelId     the related channel
 * @param offset        the date of the last message in client side
 * @param callback
 */
Server.getChannelMessages = function (channelId, offset, callback) {
    var url = GET_CHANNEL_MESSAGES
        .replace("{channelId}", encodeURIComponent(channelId))
        .replace("{offset}", encodeURIComponent(offset));
    call(GET, url, {}, callback);
};

/**
 * Send forget password request to the sever, if the all goes ok, the new password
 * changed after success question and answer for the passed username
 * @param username      the related username
 * @param question      the security question
 * @param answer        the security answer
 * @param newPassword   the new password
 * @param callback
 */
Server.forgetPassword = function (username, question, answer, newPassword, callback) {
    call(POST, FORGET_PASSWORD, {
        username: username,
        securityQuestion: question,
        securityAnswer: answer,
        newPassword: newPassword
    }, callback)
};

/**
 * Change current logged in password by passing the old password to verify
 * @param oldPassword       the current customer password
 * @param newPassword       the new customer password
 * @param callback
 */
Server.changePassword = function (oldPassword, newPassword, callback) {
    call(POST, CHANGE_PASSWORD, {
        oldPassword: oldPassword,
        newPassword: newPassword
    }, callback)
};

/**
 * Add new public/private channel with channel name and channel description
 * @param name          the new channel name
 * @param desc          the new channel description
 * @param type          the channel type {private/public}
 * @param customerId    the destination csutomer id if private
 * @param callback
 */
Server.addNewChannel = function (name, desc, type, customerId, callback) {
    call(POST, ADD_NEW_CHANNEL, {
        name: name,
        desc: desc,
        type: type,
        customerId: customerId
    }, callback)
};

/**
 * Add new channel message to the passed channel id
 * @param channelId     the related channel
 * @param message       the message to send
 * @param isReply       if reply is true add new reply to the parent message
 * @param parentId      the parent message id if reply
 * @param callback
 */
Server.addNewChannelMessage = function (channelId, message, isReply, parentId, callback) {
    call(POST, ADD_NEW_CHANNEL_MESSAGE, {
        channelId: channelId,
        message: message,
        isReply: isReply,
        parentId: parentId
    }, callback)
};


Server.readAllMessages = function (channelId, callback) {
    call(POST, READ_ALL_MESSAGES, {channelId: channelId}, callback);
};

/**
 * Subscribe the current logged in customer to the passed public channel
 * @param channelId the channel to subscribe to
 * @param callback
 */
Server.subscribeToChannel = function (channelId, callback) {
    call(POST, SUBSCRIBE_TO_CHANNEL, {
        channelId: channelId
    }, callback)
};

/**
 * Unsubscribe the current logged in customer from the passed public channel
 * @param channelId the channel to unsubscribe from
 * @param callback
 */
Server.unsubscribeFromChannel = function (channelId, callback) {
    call(POST, UNSUBSCRIBE_TO_CHANNEL, {
        channelId: channelId
    }, callback)
};

/**
 * Login to the server and save the session cookies
 * @param username
 * @param password
 * @param callback
 */
Server.login = function (username, password, callback) {
    call(POST, LOGIN_URL, {
        username: username,
        password: password,
    }, callback)
};

/**
 * Register a new customer in the system
 * @param username unique login username credential
 * @param password login password credential
 * @param nickname the customer nickname to be displayed to other users
 * @param shortDesc short description for the customer
 * @param photo link url
 * @param securityQuestion the selected question to reset password if forgot it
 * @param securityAnswer the user answer to reset password if forgot it
 * @param callback
 */
Server.signup = function (username, password, nickname, shortDesc, photo, securityQuestion, securityAnswer, callback) {
    call(POST, GET_LOGGED_IN_CUSTOMER, {
        username: username,
        password: password,
        nickname: nickname,
        shortDesc: shortDesc,
        photo: photo,
        securityQuestion: securityQuestion,
        securityAnswer: securityAnswer
    }, callback)
};

/**
 * Logout from the server
 * @param callback
 */
Server.logout = function (callback) {
    call(POST, LOGOUT_URL, {}, callback)
};

