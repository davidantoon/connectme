var app = angular.module('ConnectMeApp', []);

var ngStore;
app.factory('ConnectMeStore', function () {

    const TAG = "ConnectMeStore";
    this.store = {
        systemLoading: true,
        channels: {
            private: [],
            public: [],
            loading: true
        },
        selectedChannel: {
            channel: null,
            messages: [],
            loading: false
        },
    };
    this.listeners = [];

    this.notifyListeners = function () {
        console.log(TAG, "notifyListeners:", this.listeners.length + " listeners");
        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i]();
        }
    };

    this.registerListener = function (name, $scope) {
        console.log(TAG, "registerListener:", name);
        this.listeners.push(function () {
            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.store = this.store;
                }.bind(this));
            }.bind(this), 1);
        }.bind(this));
    };

    this.syncChannels = function (channels) {
        console.log(TAG, "syncChannels:", channels);
        this.store.channels.private = channels.privateChannels;
        this.store.channels.public = channels.publicChannels;
        this.store.channels.loading = false;
        this.notifyListeners();
    };

    this.setLoading = function (loading) {
        console.log(TAG, "setLoading:", loading);
        this.store.systemLoading = loading;
        this.notifyListeners();
    };

    this.syncCurrentUser = function (user) {
        console.log(TAG, "syncCurrentUser:", user);
        this.store.user = user;
        this.store.systemLoading = false;
        this.notifyListeners();
    };

    this.setSelectedChannel = function (channel) {
        console.log(TAG, "setSelectedChannel:", channel);
        this.store.selectedChannel.channel = channel;
        this.store.selectedChannel.loading = true;
        this.notifyListeners();
    };

    this.unSelectChannel = function () {
        console.log(TAG, "unSelectChannel");
        this.store.selectedChannel = {
            channel: null,
            messages: [],
            loading: false
        };
        this.notifyListeners();
    };

    this.setSelectedChannelMessages = function (channelId, messages) {
        console.log(TAG, "setSelectedChannelMessages:", messages);
        if (this.store.selectedChannel.channel != null &&
            this.store.selectedChannel.channel.id != channelId) {
            // to prevent slow connection lag, if we
            // open two channels quickly and the second request finished
            // before the first one, then you will see messages not related
            // to the selected channel
            return;
        }
        this.store.selectedChannel.messages = messages;
        this.store.selectedChannel.loading = false;
        this.notifyListeners();
    };

    this.addPrivateChannel = function (channel) {
        console.log(TAG, "addPrivateChannel:", channel);
        this.store.channels.private.unshift(channel);
        this.notifyListeners();
    };

    this.addChannelMessage = function (message, socket) {
        if (message.customerId == this.store.user.id && socket) {
            return;
        }
        console.log(TAG, "addChannelMessage:", message);


        var displayNotification = true;
        // check current selected channel
        if (this.store.selectedChannel.channel.id == message.channelId) {
            this.store.selectedChannel.messages.unshift(message);
            // Don't display notifications because the user in the current channel
            displayNotification = false;
        }
        // check private channels
        for (var i = 0; i < this.store.channels.private.length; i++) {
            if (this.store.channels.private[i].id == message.channelId) {
                if (displayNotification) {
                    this.store.channels.private[i].metadata.count++;
                    this.store.channels.private[i].metadata.isMentioned = ("," + message.mentions + ",").indexOf("," + this.store.user.id + ",") != -1;
                } else {
                    this.store.channels.private[i].metadata.count = 0;
                    this.store.channels.private[i].metadata.isMentioned = false;
                }
                this.store.channels.private[i].metadata.lastMessage = message;
                break;
            }
        }
        // check public channels
        for (var i = 0; i < this.store.channels.public.length; i++) {
            if (this.store.channels.public[i].id == message.channelId) {
                if (displayNotification) {
                    this.store.channels.public[i].metadata.count++;
                    this.store.channels.public[i].metadata.isMentioned = ("," + message.mentions + ",").indexOf("," + this.store.user.id + ",") != -1;
                } else {
                    this.store.channels.public[i].metadata.count = 0;
                    this.store.channels.public[i].metadata.isMentioned = false;
                }
                this.store.channels.public[i].metadata.lastMessage = message;
                break;
            }
        }

        this.notifyListeners();
    };

    return this;
});

app.factory('ConnectMeSocket', function () {
    const TAG = "ConnectMeSocket";
    const TYPES = {
        STATUS: "STATUS",
        GLOBAL: "GLOBAL",
        NEW_PRIVATE_CHANNEL: "NEW_PRIVATE_CHANNEL",
        NEW_CHANNEL_MESSAGE: "NEW_CHANNEL_MESSAGE"
    };
    this.listeners = [];

    this.isLoggedIn = false;
    this.connected = false;
    this.ws = null;
    this.onStatusListeners = [];
    this.onGlobalListeners = [];
    this.onNewPrivateChannelListeners = [];
    this.onNewChannelMessageListeners = [];

    this.notifyListeners = function (message) {
        switch (message.type) {
            case TYPES.STATUS:
                this.notifyOnStatus(message.data);
                break;
            case TYPES.GLOBAL:
                this.notifyOnGlobal(message.data);
                break;
            case TYPES.NEW_PRIVATE_CHANNEL:
                this.notifyOnNewPrivateChannel(message.data);
                break;
            case TYPES.NEW_CHANNEL_MESSAGE:
                this.notifyOnNewChannelMessage(message.data);
                break;
        }
    };

    this.registerOnStatus = function (name, callback) {
        console.log(TAG, "registerOnStatus:", name);
        this.onStatusListeners.push(callback);
    };
    this.registerOnGlobal = function (name, callback) {
        console.log(TAG, "registerOnGlobal:", name);
        this.onGlobalListeners.push(callback);
    };
    this.registerOnNewPrivateChannel = function (name, callback) {
        console.log(TAG, "registerOnNewPrivateChannel:", name);
        this.onNewPrivateChannelListeners.push(callback);
    };
    this.registerOnNewChannelMessage = function (name, callback) {
        console.log(TAG, "registerOnNewChannelMessage:", name);
        this.onNewChannelMessageListeners.push(callback);
    };
    this.notifyOnStatus = function (message) {
        console.log(TAG, "notifyOnStatusListeners:", this.onStatusListeners.length + " listeners");
        for (var i = 0; i < this.onStatusListeners.length; i++) {
            this.onStatusListeners[i](message);
        }
    };
    this.notifyOnGlobal = function (message) {
        console.log(TAG, "notifyOnGlobalListeners:", this.onGlobalListeners.length + " listeners");
        for (var i = 0; i < this.onGlobalListeners.length; i++) {
            this.onGlobalListeners[i](message);
        }
    };
    this.notifyOnNewPrivateChannel = function (message) {
        console.log(TAG, "notifyOnNewPrivateChannelListeners:", this.onNewPrivateChannelListeners.length + " listeners");
        for (var i = 0; i < this.onNewPrivateChannelListeners.length; i++) {
            this.onNewPrivateChannelListeners[i](message);
        }
    };
    this.notifyOnNewChannelMessage = function (message) {
        console.log(TAG, "notifyOnNewChannelMessageListeners:", this.onNewChannelMessageListeners.length + " listeners");
        for (var i = 0; i < this.onNewChannelMessageListeners.length; i++) {
            this.onNewChannelMessageListeners[i](message);
        }
    };

    this.onOpen = function () {
        console.log(TAG, "onOpen");
        this.connected = true;
    };

    this.onClose = function () {
        console.log(TAG, "onClose");
        this.connected = false;
        if (this.isLoggedIn) {
            console.log(TAG, "onClose", "sleep 500s and try to reconnect");
            setTimeout(function () {
                this.connect();
            }.bind(this), 500);
        }
    };

    this.onMessage = function (message) {
        try {
            this.notifyListeners(JSON.parse(message.data));
        } catch (e) {
            console.error(TAG, "onMessage", "Failed to parse message", e);
        }
    };

    this.setIsLoggedIn = function (is) {
        this.isLoggedIn = is;
    };

    this.disconnect = function () {
        this.connected = false;
        this.isLoggedIn = false;
        if (this.ws != null) {
            this.ws.close();
        }
    };
    this.connect = function () {
        if (this.connected || !this.isLoggedIn) {
            return;
        }
        this.ws = new WebSocket("ws://" + window.location.host + window.location.pathname + "socket");
        this.ws.onopen = this.onOpen.bind(this);
        this.ws.onmessage = this.onMessage.bind(this);
        this.ws.onclose = this.onClose.bind(this);
        this.ws.onerror = function (e) {
            console.log(TAG, "onError", e);
        }.bind(this);
    };

    this.refreshConnection = function () {
        this.disconnect();
        setTimeout(function () {
            this.connected = false;
            this.isLoggedIn = true;
            this.connect();
        }.bind(this), 500);
    };
    return this;
});

app.controller('MainController', ['$scope', 'ConnectMeStore', 'ConnectMeSocket', function ($scope, $store, $socket) {
    ngStore = $store;
    var TAG = "MainController";
    console.log("*********************************\nMainController Init\n*********************************");
    $scope.store = $store.store;
    $scope.section = 0;
    $scope.searchChannelText = "";
    $scope.searchChannelResults = [];
    $store.registerListener(TAG, $scope);
    $socket.registerOnGlobal(TAG, function (message) {
        console.log(TAG, "On socket message", message);
    });
    $socket.registerOnStatus(TAG, function (message) {
        console.log(TAG, "Socket Status", message);
    });
    $socket.registerOnNewChannelMessage(TAG, function (message) {
        $store.addChannelMessage(message, true);
    });
    $socket.registerOnNewPrivateChannel(TAG, function (message) {
        console.log(TAG, "On socket message", message);
        $store.addPrivateChannel(message);
    });


    $scope.setSection = function (s) {
        $scope.section = s;
    };

    $scope.login = function () {
        $store.setLoading(true);
        Server.login($('#username').val(), $('#password').val(), function (result, error) {
            if (error != null) {
                $store.setLoading(false);
                alert(error.message);
                return;
            }
            $scope.refreshUserData();
            setTimeout(function () {
                $store.setLoading(false);
            }, 300);
        });
    };

    $scope.signup = function () {
        var username = $('#singup_username').val();
        var nickname = $('#singup_nickname').val();
        var shortDesc = $('#singup_shortDesc').val();
        var photo = $('#singup_photo').val();
        var securityQuestion = $('#singup_securityQuestion').val();
        var securityAnswer = $('#singup_securityAnswer').val();
        var password = $('#singup_password').val();
        var confirmPassword = $('#singup_confirmPassword').val();

        if (password != confirmPassword) {
            alert("Confirm password not match");
            return;
        }

        if (username.indexOf(" ") != -1) {
            alert("Username must not contain spaces, try: " + nickname.replace(/ /g, "_"));
            return;
        }

        if (nickname.indexOf(" ") != -1) {
            alert("Nickname must not contain spaces, try: " + nickname.replace(/ /g, "_"));
            return;
        }

        $store.setLoading(true);
        Server.signup(username, password, nickname, shortDesc, photo, securityQuestion, securityAnswer, function (success, error) {
            if (error != null) {
                $store.setLoading(false);
                alert(error.message);
                return;
            }
            Server.login(username, password, function (result, error) {
                if (error != null) {
                    $store.setLoading(false);
                    alert(error.message);
                    return;
                }
                $scope.refreshUserData();
                setTimeout(function () {
                    $store.setLoading(false);
                }, 300);
            });
        });
    };


    $scope.recoverPassword = function () {

        var username = $('#forget_username').val();
        var securityQuestion = $('#forget_securityQuestion').val();
        var securityAnswer = $('#forget_securityAnswer').val();
        var newPassword = $('#forget_newPassword').val();

        Server.forgetPassword(username, securityQuestion, securityAnswer, newPassword, function (success, error) {
            if (error != null) {
                $store.setLoading(false);
                alert(error.message);
                return;
            }
            Server.login(username, newPassword, function (result, error) {
                if (error != null) {
                    $store.setLoading(false);
                    alert(error.message);
                    return;
                }
                $scope.refreshUserData();
                setTimeout(function () {
                    $store.setLoading(false);
                }, 300);
            });
        });
    };

    $scope.logout = function () {
        $.ajax({url: "api/logout", method: "POST"});
        $socket.setIsLoggedIn(false);
        $socket.disconnect();
        window.location.reload();
    };

    $scope.refreshUserData = function () {
        Server.getUserData(function (result, error) {
            if (error) {
                console.error(TAG, "User not logged in");
                // $store.syncCurrentUser({nickname: "debug"});
                $store.setLoading(false);
                return;
            }
            console.log(TAG, "Logged in user", result);
            setTimeout(function () {
                $store.syncCurrentUser(result);
                $socket.setIsLoggedIn(true);
                $socket.connect(true);
                $scope.loadUserContent();
            }, 500);
        });
    };
    $scope.refreshUserData();

    $scope.loadUserContent = function () {
        Server.getMyChannels(function (channels, error) {
            if (error != null) {
                alert(error.message);
                return;
            }
            $store.syncChannels(channels);
        })
    };

    $scope.searchChannels = function () {
        var searchText = $('#searchPublicChannels').val();
        if (searchText.trim().length == 0) {
            return;
        }
        $scope.searchChannelText = searchText;
        $scope.searchChannelResults = [];
        console.log(TAG, "searchChannels", searchText);
        Server.searchChannels(searchText, function (success, error) {
            if (error != null) {
                alert(error.message);
                return;
            }
            apply($scope, function () {
                $scope.searchChannelResults = success;
            });

            var fakeId = 'fakeButton' + new Date().getTime();
            $('body').append('<a id="' + fakeId + '" data-toggle="modal" data-target="#searchResultPublicChannelModal"></a>');
            $('#' + fakeId)[0].click();
            $('#' + fakeId).remove();
        })
    };

    $scope.getDateFormat = getDateFormat;

    $scope.subscribeToChannel = function (channel) {
        console.log(TAG, "subscribeToChannel", channel);
        Server.subscribeToChannel(channel.id, function (success, error) {
            if (error != null) {
                alert(error.message);
                return;
            }
            $store.syncChannels(success);
            $socket.refreshConnection();
            $('#closeSearchResultPublicChannelModal')[0].click();
            openChannel(channel.id);
        });
    };


    // On username clicked open private channel if not exists
    $('body').on("click", ".user-link", function () {
        // check if the clicked username is own username
        if ($(this).attr("customerId") == $store.store.user.username) {
            return;
        }

        // check if private channel exists
        var privateChannels = $store.store.channels.private;
        for (var i = 0; i < privateChannels.length; i++) {
            if (privateChannels[i].subscribers[0] == $store.store.user.id ||
                privateChannels[i].subscribers[1] == $store.store.user.id) {
                // private channel exists
                openChannel(privateChannels[i].id);
                return;
            }
        }
        // create private channel
        Server.addNewChannel("", "", 1, $scope.selectedCustomer.id, function (success, error) {
            if (error != null) {
                alert(error.message);
                return;
            }
            $store.syncChannels(success);
            $socket.refreshConnection();
            openChannel(success.privateChannels[0].id);
        });
        console.log(this);
    });
}]);

app.controller('ChannelListController', ['$scope', 'ConnectMeStore', 'ConnectMeSocket', '$sce', function ($scope, $store, $socket, $sce) {
    var TAG = "ChannelListController";
    console.log("*********************************\nChannelListController Init\n*********************************");
    $scope.store = $store.store;
    $store.registerListener(TAG, $scope);

    $scope.getHtml = function (html) {
        return $sce.trustAsHtml(html);
    };

    $scope.selectChannel = function (channel) {
        $store.setSelectedChannel(channel);
        Server.getChannelMessages(channel.id, new Date().getTime(), function (messages, error) {
            if (error != null) {
                alert(error.message);
                $store.setSelectedChannel(null);
                return;
            }
            $store.setSelectedChannelMessages(channel.id, messages);
            Server.readAllMessages(channel.id, function (channels, error) {
                if (error == null) {
                    $store.syncChannels(channels);
                }
            })
        });
    };

}]);

app.controller('ChannelViewController', ['$scope', 'ConnectMeStore', 'ConnectMeSocket', '$sce', function ($scope, $store, $socket, $sce) {
    var TAG = "ChannelViewController";
    console.log("*********************************\nChannelViewController Init\n*********************************");
    $scope.store = $store.store;
    $store.registerListener(TAG, $scope);


    $scope.closeChannel = function () {
        $store.unSelectChannel();
    };

    $socket.registerOnNewChannelMessage(TAG, function (message) {
        console.log(TAG, "registerOnNewChannelMessage", message);
        if (message.channelId == $store.store.selectedChannel.channel.id) {
            Server.readAllMessages(message.channelId, function (channels, error) {
                if (error == null) {
                    $store.syncChannels(channels);
                }
            })
        }
    });


    $scope.sendMessage = function () {
        if ($('#sendMessageButton').val() == "")
            return;
        Server.addNewChannelMessage($store.store.selectedChannel.channel.id,
            $('#sendMessageButton').val(), false, null, function (newMessage, error) {
                if (error != null) {
                    alert(error.message);
                    return;
                }
                $('#sendMessageButton').val("");
                $store.addChannelMessage(newMessage, false);
            }
        )
    };


    $scope.getDateFormat = getDateFormat;

    $scope.getHtml = function (html) {
        return $sce.trustAsHtml(html);
    };

    $scope.replayToMessage = function (message) {
        $('#sendMessageButton').val("@" + message.customer.nickname + ": ");
        $('#sendMessageButton').focus();
    };


    $scope.unsubscribeChannel = function (channel) {
        console.log(TAG, "unsubscribeChannel", channel);
        Server.unsubscribeFromChannel(channel.id, function (success, error) {
            if (error != null) {
                alert(error.message);
                return;
            }
            $store.syncChannels(success);
            $scope.closeChannel();
            $socket.refreshConnection();
        });
    };

}]);


app.controller('ChannelDialogController', ['$scope', 'ConnectMeStore', 'ConnectMeSocket', function ($scope, $store, $socket) {
    var TAG = "ChannelDialogController";
    console.log("*********************************\nChannelDialogController Init\n*********************************");

    $scope.customers = [];
    $scope.selectedCustomer = null;
    $scope.searchValue = "";

    $scope.createPublicChannelModal = function () {
        console.log(TAG, "createPublicChannelModal", "called");
        var channel_name = $('#public_channel_name').val();
        var channel_desc = $('#public_channel_desc').val();

        Server.addNewChannel(channel_name, channel_desc, 0, null, function (success, error) {
            if (error != null) {
                alert(error.message);
                return;
            }

            $('#closePublicModal')[0].click();
            $store.syncChannels(success);
            $socket.refreshConnection();
            openChannel(success.publicChannels[0].id);
        });
    };
    $scope.createPrivateChannelModal = function () {
        console.log(TAG, "createPrivateChannelModal", "called");

        if ($scope.selectedCustomer == null) {
            alert("Search for customers and select one");
            return;
        }

        Server.addNewChannel("", "", 1, $scope.selectedCustomer.id, function (success, error) {
            if (error != null) {
                alert(error.message);
                return;
            }

            $('#closePrivateModal')[0].click();
            $store.syncChannels(success);
            $socket.refreshConnection();
            openChannel(success.privateChannels[0].id);
        });
    };


    $scope.$watch("searchValue", function (newVal) {
        console.log(TAG, "searchValueWatch", newVal);
        if (newVal.trim().length == 0) {
            apply($scope, function () {
                $scope.customers = [];
                $scope.selectedCustomer = null;
            });
        } else {
            $scope.searchCustomer(newVal);
        }
    });
    $scope.searchCustomer = function (text) {
        console.log(TAG, "searchCustomer", text);
        Server.searchCustomers(text, function (customers, error) {
            if (error != null) {
                customers = [];
            }
            apply($scope, function () {
                $scope.customers = customers;
                $scope.selectedCustomer = null;
            });
        });
    };

    $scope.selectCustomer = function (customer) {
        $scope.selectedCustomer = customer;
    };
}]);


function getDateFormat(date) {

    var d = new Date(date);
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = (d.getFullYear() + "").substring(2);

    hours = (hours < 10 ? "0" : "") + hours;
    minutes = (minutes < 10 ? "0" : "") + minutes;
    seconds = (seconds < 10 ? "0" : "") + seconds;
    day = (day < 10 ? "0" : "") + day;
    month = (month < 10 ? "0" : "") + month;

    // HH:MM:SS dd/mm/yy
    return hours + ":" + minutes + ":" + seconds + " " + day + "/" + month + "/" + year;
}
function apply(s, f) {
    setTimeout(function () {
        s.$apply(f);
    }, 1);
}


function openChannel(id) {
    setTimeout(function () {
        $('#channelButton-' + id)[0].click();
    }, 500);
}

$(window).ready(function () {
    $('body').on("keyup", "#username, #password", function (e) {
        if (e.keyCode == 13) {
            $('#loginButton')[0].click();
        }
    });
});












