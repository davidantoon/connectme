package com.connectme.enums;

public enum ChannelType {
    PUBLIC,
    PRIVATE,
}
