package com.connectme.enums;

public enum ChannelNotificationType {
    NEW_PRIVATE_CHANNEL,
    NEW_CHANNEL_MESSAGE,
}
