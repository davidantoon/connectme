package com.connectme.enums;

public enum MessageType {
    REGULAR,
    REPLY,
    CONTROL,
}
