package com.connectme.exception;

public class CustomerNotSubscribedToChannel extends ConnectMeException {
    public CustomerNotSubscribedToChannel() {
        super("Customer not subscribed to this channel", ErrorCodes.CUSTOMER_NOT_SUBSCRIBED_TO_CHANNEL);
    }
}
