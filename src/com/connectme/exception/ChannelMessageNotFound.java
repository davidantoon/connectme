package com.connectme.exception;

public class ChannelMessageNotFound extends ConnectMeException {
    public ChannelMessageNotFound() {
        super("Channel Message Not Found", ErrorCodes.CHANNEL_MESSAGE_NOT_FOUND);
    }
}
