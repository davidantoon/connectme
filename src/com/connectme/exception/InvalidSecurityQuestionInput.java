package com.connectme.exception;

public class InvalidSecurityQuestionInput extends ConnectMeException {
    public InvalidSecurityQuestionInput() {
        super("Invalid security question input, must not be empty", ErrorCodes.INVALID_SECURITY_QUESTION_INPUT);
    }
}