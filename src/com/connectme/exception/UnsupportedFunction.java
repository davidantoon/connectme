package com.connectme.exception;

public class UnsupportedFunction extends ConnectMeException {

    public UnsupportedFunction() {
        super("Unsupported function", 401);
    }
}
