package com.connectme.exception;

public class InvalidShortDescInput extends ConnectMeException {
    public InvalidShortDescInput() {
        super("Invalid short description input, must be up to 50 characters", ErrorCodes.INVALID_PASSWORD_INPUT);
    }
}