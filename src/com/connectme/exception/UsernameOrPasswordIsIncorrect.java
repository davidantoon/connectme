package com.connectme.exception;

public class UsernameOrPasswordIsIncorrect extends ConnectMeException {

    public UsernameOrPasswordIsIncorrect() {
        super("Username or password is incorrect!", 403);
    }
}
