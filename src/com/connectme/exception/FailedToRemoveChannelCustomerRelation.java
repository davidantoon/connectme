package com.connectme.exception;

public class FailedToRemoveChannelCustomerRelation extends ConnectMeException {

    public FailedToRemoveChannelCustomerRelation() {
        super("Failed to unsubsribe from channel", ErrorCodes.FAILED_TO_REMOVE_CHANNEL_CUSTOMER_RELATION);
    }
}
