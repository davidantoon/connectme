package com.connectme.exception;

public class ChannelCustomerRelationNotFound extends ConnectMeException {
    public ChannelCustomerRelationNotFound() {
        super("Customer channel relation not found", ErrorCodes.CHANNEL_CUSTOMER_RELATION_NOT_FOUND);
    }
}
