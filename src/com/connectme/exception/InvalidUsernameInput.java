package com.connectme.exception;

public class InvalidUsernameInput extends ConnectMeException {
    public InvalidUsernameInput() {
        super("Invalid username input, must be not empty and up to 10 characters", ErrorCodes.INVALID_USERNAME_INPUT);
    }
}