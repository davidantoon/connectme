package com.connectme.exception;

public class ChannelNotFound extends ConnectMeException {

    public ChannelNotFound() {
        super("Channel not found", ErrorCodes.CHANNEL_NOT_FOUND);
    }
}
