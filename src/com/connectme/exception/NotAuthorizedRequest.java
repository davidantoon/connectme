package com.connectme.exception;

public class NotAuthorizedRequest extends ConnectMeException {
    public NotAuthorizedRequest() {
        super("Not Authorized!", 403);
    }
}
