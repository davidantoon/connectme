package com.connectme.exception;

public class MessageMustBeNotEmptyAndLessThan500 extends ConnectMeException {
    public MessageMustBeNotEmptyAndLessThan500() {
        super("Message must be not empty and less than 500", ErrorCodes.MESSAGE_MUST_BENOT_EMPTY_AND_LESS_THAN_500);
    }
}
