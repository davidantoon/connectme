package com.connectme.exception;

public class FailedToAddUserException extends ConnectMeException {

    public FailedToAddUserException() {
        super("Failed to add user",ErrorCodes.FAILED_TO_ADD_USER_EXCEPTION);
    }
}
