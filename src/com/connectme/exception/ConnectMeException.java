package com.connectme.exception;


import com.connectme.dao.CustomerDao;
import com.connectme.dao.CustomerDaoImpl;

/**
 * Main Exception Class contains the exception runtime message and custom error code
 */
public class ConnectMeException extends RuntimeException {

    private Integer code;

    public ConnectMeException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;

    }
}
