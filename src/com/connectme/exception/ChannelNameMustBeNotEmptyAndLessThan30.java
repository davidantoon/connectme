package com.connectme.exception;

import com.connectme.ConnectMeApp;

public class ChannelNameMustBeNotEmptyAndLessThan30 extends ConnectMeException {

    public ChannelNameMustBeNotEmptyAndLessThan30() {
        super("Channel name must be not empty and less than 30", ErrorCodes.CHANNEL_NAME_MUST_BENOT_EMPTY_AND_LESS_THAN_30);
    }
}
