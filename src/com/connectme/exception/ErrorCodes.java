package com.connectme.exception;

public class ErrorCodes {
    public static final Integer CUSTOMER_NOT_FOUND = 10001;
    public static final Integer FAILED_TO_ADD_USER_EXCEPTION = 10002;
    public static final Integer INVALID_SECURITY_ANSWER_INPUT = 10003;
    public static final Integer INVALID_PASSWORD_INPUT = 10004;
    public static final Integer INVALID_NICKNAME_INPUT = 10005;
    public static final Integer INVALID_SECURITY_QUESTION_INPUT = 10006;
    public static final Integer INVALID_USERNAME_INPUT = 10007;
    public static final Integer USERNAME_ALREADY_EXISTS = 10008;
    public static final Integer INCORRECT_OLD_PASSWORD_INPUT = 10009;
    public static final Integer CHANNEL_CUSTOMER_RELATION_NOT_FOUND = 10010;
    public static final Integer FAILED_TO_REMOVE_CHANNEL_CUSTOMER_RELATION = 10011;
    public static final Integer CHANNEL_CUSTOMER_RELATION_ALREADY_EXISTS = 10012;
    public static final Integer CHANNEL_NOT_FOUND = 10013;
    public static final Integer CUSTOMER_NOT_SUBSCRIBED_TO_CHANNEL = 10014;
    public static final Integer REPLIED_MESSAGE_OWNER_NOT_SUBSCRIBED_TO_CHANNEL = 10015;
    public static final Integer CHANNEL_MESSAGE_NOT_FOUND = 10016;
    public static final Integer CHANNEL_NAME_MUST_BENOT_EMPTY_AND_LESS_THAN_30 = 10016;
    public static final Integer MESSAGE_MUST_BENOT_EMPTY_AND_LESS_THAN_500 = 10017;
}
