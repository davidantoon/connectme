package com.connectme.exception;

public class InvalidSecurityAnswerInput extends ConnectMeException {
    public InvalidSecurityAnswerInput() {
        super("Invalid security answer input, must not be empty", ErrorCodes.INVALID_SECURITY_ANSWER_INPUT);
    }
}
