package com.connectme.exception;

import com.connectme.ConnectMeApp;

public class UsernameAlreadyExists extends ConnectMeException {

    public UsernameAlreadyExists() {
        super("Username already exists, choose other one or click on forget password.", ErrorCodes.USERNAME_ALREADY_EXISTS);
    }
}
