package com.connectme.exception;

public class ChannelCustomerRelationAlreadyExists extends ConnectMeException {

    public ChannelCustomerRelationAlreadyExists() {
        super("Channel customer relation already exists", ErrorCodes.CHANNEL_CUSTOMER_RELATION_ALREADY_EXISTS);
    }
}
