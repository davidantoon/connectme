package com.connectme.exception;

public class InvalidNicknameInput extends ConnectMeException {
    public InvalidNicknameInput() {
        super("Invalid nickname input, must be not empty and up to 20 characters", ErrorCodes.INVALID_NICKNAME_INPUT);
    }
}