package com.connectme.exception;

public class CustomerNotFound extends ConnectMeException {

    public CustomerNotFound() {
        super("Customer not found", ErrorCodes.CUSTOMER_NOT_FOUND);
    }
}
