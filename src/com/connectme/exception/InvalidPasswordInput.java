package com.connectme.exception;

public class InvalidPasswordInput extends ConnectMeException {
    public InvalidPasswordInput() {
        super("Invalid password input, must be not empty and up to 8 characters", ErrorCodes.INVALID_PASSWORD_INPUT);
    }
}