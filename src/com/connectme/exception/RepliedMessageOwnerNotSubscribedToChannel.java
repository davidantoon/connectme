package com.connectme.exception;

public class RepliedMessageOwnerNotSubscribedToChannel extends ConnectMeException {
    public RepliedMessageOwnerNotSubscribedToChannel() {
        super("Replied message's owner not subscribed to this channel", ErrorCodes.REPLIED_MESSAGE_OWNER_NOT_SUBSCRIBED_TO_CHANNEL);
    }
}
