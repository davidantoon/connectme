package com.connectme.exception;

public class UnknownException extends ConnectMeException {

    public static final String MESSAGE = "500 Server Error";

	public UnknownException() {
        super(MESSAGE, 500);
    }
}
