package com.connectme.exception;

public class IncorrectOldPasswordInput extends ConnectMeException {
    public IncorrectOldPasswordInput() {
        super("Incorrect password, please enter your current password and the new password", ErrorCodes.INCORRECT_OLD_PASSWORD_INPUT);
    }
}