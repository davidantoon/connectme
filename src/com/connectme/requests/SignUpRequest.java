package com.connectme.requests;

public class SignUpRequest {

    public String username;
    public String password;
    public String nickname;
    public String shortDesc;
    public String photo;
    public String securityQuestion;
    public String securityAnswer;

    public SignUpRequest() {
    }

    public SignUpRequest(String username, String password, String nickname, String shortDesc, String photo, String securityQuestion, String securityAnswer) {
        this.username = username;
        this.password = password;
        this.nickname = nickname;
        this.shortDesc = shortDesc;
        this.photo = photo;
        this.securityQuestion = securityQuestion;
        this.securityAnswer = securityAnswer;
    }

    @Override
    public String toString() {
        return "SignUpRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", shortDesc='" + shortDesc + '\'' +
                ", photo='" + photo + '\'' +
                ", securityQuestion='" + securityQuestion + '\'' +
                ", securityAnswer='" + securityAnswer + '\'' +
                '}';
    }
}
