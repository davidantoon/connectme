package com.connectme.requests;

public class ForgetPasswordRequest {
    public String username;
    public String securityQuestion;
    public String securityAnswer;
    public String newPassword;

    public ForgetPasswordRequest() {
    }

    public ForgetPasswordRequest(String username, String securityQuestion, String securityAnswer, String newPassword) {
        this.username = username;
        this.securityQuestion = securityQuestion;
        this.securityAnswer = securityAnswer;
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "ForgetPasswordRequest{" +
                "username='" + username + '\'' +
                ", securityQuestion='" + securityQuestion + '\'' +
                ", securityAnswer='" + securityAnswer + '\'' +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }

}
