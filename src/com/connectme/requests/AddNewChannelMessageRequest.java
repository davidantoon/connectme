package com.connectme.requests;

public class AddNewChannelMessageRequest {
    public Integer channelId;
    public String message;
    public Boolean isReply;
    public Integer parentId;

    public AddNewChannelMessageRequest(Integer channelId, String message, Boolean isReply, Integer parentId) {
        this.channelId = channelId;
        this.message = message;
        this.isReply = isReply;
        this.parentId = parentId;
    }
}
