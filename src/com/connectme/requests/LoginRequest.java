package com.connectme.requests;

import com.connectme.util.ValidateInput;

public class LoginRequest {
    public String username;
    public String password;

    public LoginRequest() {
    }

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public boolean isValid() {
        return ValidateInput.isValidUsername(username) && ValidateInput.isValidPassword(password);
    }

    @Override
    public String toString() {
        return "LoginRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
