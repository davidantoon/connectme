package com.connectme.requests;

import com.connectme.model.Channel;
import com.google.gson.JsonElement;

import java.util.List;

public class ChannelResponse {

    private List<Channel> privateChannels;
    private List<Channel> publicChannels;

    public ChannelResponse(List<Channel> privateChannels, List<Channel> publicChannels) {
        this.privateChannels = privateChannels;
        this.publicChannels = publicChannels;
    }

    public List<Channel> getPrivateChannels() {
        return privateChannels;
    }

    public void setPrivateChannels(List<Channel> privateChannels) {
        this.privateChannels = privateChannels;
    }

    public List<Channel> getPublicChannels() {
        return publicChannels;
    }

    public void setPublicChannels(List<Channel> publicChannels) {
        this.publicChannels = publicChannels;
    }
}
