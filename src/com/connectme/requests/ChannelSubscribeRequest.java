package com.connectme.requests;

public class ChannelSubscribeRequest {
    public Integer channelId;

    public ChannelSubscribeRequest(Integer channelId) {
        this.channelId = channelId;
    }
}
