package com.connectme.requests;

public class SuccessResponse {
    public String message;
    public Integer code;

    public SuccessResponse(String message) {
        this.message = message;
        this.code = 200;
    }
}
