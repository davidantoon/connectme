package com.connectme.requests;

public class AddNewChannelRequest {
    public String name;
    public String desc;
    public Integer type;
    public Integer customerId;

    public AddNewChannelRequest(String name, String desc, Integer type, Integer customerId) {
        this.name = name;
        this.desc = desc;
        this.type = type;
        this.customerId = customerId;
    }
}
