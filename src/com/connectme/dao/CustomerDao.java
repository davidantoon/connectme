package com.connectme.dao;

import com.connectme.exception.*;
import com.connectme.model.Customer;
import com.connectme.requests.ChangePasswordRequest;
import com.connectme.requests.ForgetPasswordRequest;
import com.connectme.requests.LoginRequest;
import com.connectme.requests.SignUpRequest;

import java.util.List;

public interface CustomerDao {


    /**
     * Get all customers from database, hide passwords
     *
     * @return {@link List<Customer>}
     */
    List<Customer> getAllCustomers(Boolean hideSensitiveData) throws Exception;

    /**
     * Get customer by customer id
     *
     * @param customerId customer id to find
     * @return {@link Customer} if exists
     * @throws CustomerNotFound if not found
     */
    Customer getCustomerById(Long customerId, Boolean hideSensitiveData) throws Exception;

    /**
     * Check if username and password is correct
     *
     * @param loginRequest {@link LoginRequest} from client side
     * @return {@link Customer} object if correct
     * @throws UsernameOrPasswordIsIncorrect if password or username isn't correct
     */
    Customer checkCredential(LoginRequest loginRequest) throws Exception;

    /**
     * Register new customer in the database, check valid inputs and throws runtime exception
     * related to the invalid field:
     * - Username: not empty, unique and up to 10 characters
     * - Password: not empty and up to 8 characters
     * - Nickname: not empty and up to 20 characters
     * - Short Description: up to 50 characters
     * - Security Question: not empty
     * - Security Answer: not empty
     *
     * @param signUpRequest {@link SignUpRequest} from client side
     * @return {@link Customer} new customer object if succeeded
     * @throws InvalidNicknameInput         if the Nickname does not match the requirements
     * @throws InvalidPasswordInput         if the Password does not match the requirements
     * @throws InvalidSecurityAnswerInput   if the SecurityAnswer does not match the requirements
     * @throws InvalidSecurityQuestionInput if the SecurityQuestion does not match the requirements
     * @throws InvalidShortDescInput        if the ShortDesc does not match the requirements
     * @throws InvalidUsernameInput         if the Username does not match the requirements
     */
    Customer addNewCustomer(SignUpRequest signUpRequest) throws Exception;

    /**
     * Get {@link Customer} object from database where by customer's username
     *
     * @param username the customer's username
     * @return {@link Customer} object if exists
     * @throws CustomerNotFound if no customer matches the passed username
     */
    Customer getCustomerByUsername(String username, Boolean hideSensitiveData) throws Exception;

    /**
     * Checks if the old password correct and replace it with the new password, validate new input:
     * - Password: not empty and up to 8 characters
     *
     * @param changePasswordRequest {@link ChangePasswordRequest} from client side
     * @param customerId            the customer id form the session, to prevent changing password for other users
     * @throws IncorrectOldPasswordInput if the old password not matches
     * @throws InvalidPasswordInput      if the new password is empty or more than 8 characters
     */
    void changePassword(ChangePasswordRequest changePasswordRequest, Long customerId) throws Exception;

    /**
     * Checks if the security question, security answer is correct for the passed username,
     * then update the customer password.
     *
     * @param forgetPasswordRequest {@link ForgetPasswordRequest} from client side
     * @throws CustomerNotFound     if username not belong to any customer in the database
     * @throws InvalidPasswordInput if the passed password not match the requirements
     */
    void forgetPassword(ForgetPasswordRequest forgetPasswordRequest) throws Exception;

    /**
     * Retrieve customer object by nickname
     *
     * @param nickname search nickname
     * @throws CustomerNotFound if not exists
     */
    Customer getCustomerByNickname(String nickname) throws Exception;

    /**
     * Search in all customer by customer nickname
     *
     * @param searchText the search text
     * @return {@link List<Customer>} matched the search text
     * @throws Exception 
     */
    List<Customer> searchCustomers(String searchText) throws Exception;
}
