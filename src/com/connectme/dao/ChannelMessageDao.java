package com.connectme.dao;

import com.connectme.exception.ChannelMessageNotFound;
import com.connectme.exception.CustomerNotSubscribedToChannel;
import com.connectme.exception.RepliedMessageOwnerNotSubscribedToChannel;
import com.connectme.model.*;

import java.util.List;

public interface ChannelMessageDao {


    /**
     * Get all channel message and by created at > offset to support paging requests, limit 10 messages
     * per page, select from all messages where {@link ChannelCustomerRelation} for the passed customer
     * greater than the relation's created at date to show only the messages after the customer subscription
     * date
     *
     * @param customer  request customer
     * @param channelId the related channel
     * @param offset    date offset from the last client side {@link ChannelMessage#createdAt} date
     * @return The first 10 {@link ChannelMessage} as list after the passed offset date
     */
    List<ChannelMessage> getMassageByChannelAndOffset(Customer customer, Long channelId, Long offset) throws Exception;

    /**
     * Update all {@link ChannelMessage#readBy} and append the passed customer id, update the messages
     * where the {@link ChannelMessage#createdAt} greater than the {@link ChannelCustomerRelation#createdAt}
     * to prevent mark read for pre subscribe sent messages
     *
     * @param customer  request customer
     * @param channelId the related channel
     */
    void readAllMessagesInChannel(Customer customer, Long channelId) throws Exception;

    /**
     * Count all the messages that {@link ChannelMessage#readBy} does not contains the passed customer id
     * and the {@link ChannelMessage#createdAt} greater than the {@link ChannelCustomerRelation#createdAt}
     * for the passed customer
     *
     * @param customer  request customer
     * @param channelId the related channel
     * @return {@link ChannelMessageMeta} info about the number of unread message by {@link Channel} and {@link Customer},
     * the last message in the channel and if the passed customer mentioned in the unread messages
     */
    ChannelMessageMeta getUnreadMessagesForCustomerByChannel(Customer customer, Long channelId) throws Exception;

    /**
     * Add new message to passed channel, scan the message input and mentions that starts
     * with '@' and ends with 'space', '.' or ',' checks if the current mentions customer exists,
     * and if the current mention customer in subscribed to this channel to notify them.
     * After adding the message to the database notify all subscribed customers.
     *
     * @param channel  the related channel
     * @param customer sender customer
     * @param message  the content of the message
     * @return The new {@link ChannelMessage} if successfully added
     * @throws CustomerNotSubscribedToChannel if sender customer not subscribed to prevent non-subscribers to send messages
     */
    ChannelMessage addNewMessage(Channel channel, Customer customer, String message) throws Exception;


    /**
     * Add new control message to display the subscriptions history of the channel,
     * notify channel subscribers if notify = true
     *
     * @param channel  the related channel
     * @param notify   if to notify subscribers
     * @param customer sender customer
     * @param message  control message content
     * @return The new {@link ChannelMessage} if successfully added
     */
    ChannelMessage addNewControlMessage(Channel channel, boolean notify, Customer customer, String message) throws Exception;

    /**
     * Get {@link ChannelMessage} if exists
     *
     * @param id channel message id
     * @return {@link ChannelMessage} if exists
     * @throws ChannelMessageNotFound if not exists
     */
    ChannelMessage getMessageById(Long id) throws Exception;
}
