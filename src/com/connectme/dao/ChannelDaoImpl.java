package com.connectme.dao;

import com.connectme.ConnectMeApp;
import com.connectme.enums.ChannelNotificationType;
import com.connectme.enums.ChannelType;
import com.connectme.exception.ChannelNameMustBeNotEmptyAndLessThan30;
import com.connectme.exception.ChannelNotFound;
import com.connectme.model.*;
import com.connectme.util.Log;
import com.connectme.util.OT;
import com.connectme.util.SqlStmt;
import com.connectme.util.ValidateInput;

import java.nio.channels.Channels;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class ChannelDaoImpl implements ChannelDao {

    private static final String TAG = ChannelDaoImpl.class.getSimpleName();

    @Override
    public List<Channel> getAllCustomerChannelsByType(Customer customer, ChannelType type) throws Exception {

        Log.l(TAG, "getAllCustomerChannels", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_ALL_CUSTOMER_CHANNELS_BY_TYPE);
            statement.setLong(1, type.ordinal());
            statement.setLong(2, customer.getId());

            rs = statement.executeQuery();
            List<Channel> channels = new ArrayList<>();
            while (rs.next()) {
                if (type == ChannelType.PRIVATE) {

                    // Retrieve the other customer name
                    List<ChannelCustomerRelation> relation = FactoryDao.getChannelCustomerRelationDao()
                            .getChannelCustomerRelationByChannelId(rs.getLong("id"));
                    String customerName = relation.get(0).getCustomerId().equals(customer.getId()) ?
                            FactoryDao.getCustomerDao().getCustomerById(relation.get(1).getCustomerId(), true).getNickname()
                            : FactoryDao.getCustomerDao().getCustomerById(relation.get(0).getCustomerId(), true).getNickname();

                    channels.add(new PrivateChannel(
                            rs.getLong("id"),
                            customerName,
                            rs.getString("shortDesc"),
                            rs.getLong("createdByCustomerId"),
                            rs.getLong("createdAt"),
                            rs.getLong("updatedAt")
                    ));
                } else {
                    channels.add(new PublicChannel(
                            rs.getLong("id"),
                            rs.getString("name"),
                            rs.getString("shortDesc"),
                            rs.getLong("createdByCustomerId"),
                            rs.getLong("createdAt"),
                            rs.getLong("updatedAt")
                    ));
                }
            }

            for (Channel channel : channels) {
                List<ChannelCustomerRelation> relations = FactoryDao.getChannelCustomerRelationDao().getChannelCustomerRelationByChannelId(channel.getId());
                List<Long> subscribers = new ArrayList<>();
                for (ChannelCustomerRelation relation : relations) {
                    subscribers.add(relation.getCustomerId());
                }
                channel.setSubscribers(subscribers);
                channel.setMetadata(FactoryDao.getChannelMessageDao().getUnreadMessagesForCustomerByChannel(customer, channel.getId()));
            }

            Collections.sort(channels, new Comparator<Channel>() {
                @Override
                public int compare(Channel o1, Channel o2) {
                    Long date1 = o1.getCreatedAt();
                    Long date2 = o2.getCreatedAt();
                    if (o1.getMetadata().getLastMessage() != null) {
                        date1 = o1.getMetadata().getLastMessage().getCreatedAt();
                    }
                    if (o2.getMetadata().getLastMessage() != null) {
                        date2 = o2.getMetadata().getLastMessage().getCreatedAt();
                    }
                    return date1 < date2 ? 1 : -1;
                }
            });

            Log.l(TAG, "getAllCustomerChannels", "Customer Channels cound: " + channels.size());
            return channels;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getAllCustomerChannels", "Finished" + t.close());
        }
    }

    @Override
    public List<Channel> createNewPublicChannel(Customer customer, String name, String shortDesc) throws Exception {
        Log.l(TAG, "createNewPublicChannel", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            if (!ValidateInput.isInRange(name, 1, 30)) {
                throw new ChannelNameMustBeNotEmptyAndLessThan30();
            }
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, name);
            statement.setString(2, shortDesc);
            statement.setLong(3, customer.getId());
            statement.setLong(4, new Date().getTime());
            statement.setLong(5, new Date().getTime());
            statement.setInt(6, ChannelType.PUBLIC.ordinal());
            statement.executeUpdate();

            rs = statement.getGeneratedKeys(); // will return the generated id
            rs.next();
            Log.l(TAG, "createNewPublicChannel",
                    "New public channel added successfully name: " + name + " by: " + customer.getNickname());

            Log.l(TAG, "createNewPublicChannel", "Adding new channel customer relation for the creator");
            FactoryDao.getChannelCustomerRelationDao().addCustomerToPublicChannel(customer, (PublicChannel) getChannelById(rs.getLong(1)));

            connection.commit();
            return getAllCustomerChannelsByType(customer, ChannelType.PUBLIC);
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "createNewPublicChannel", "Finished" + t.close());
        }
    }

    @Override
    public List<Channel> createNewPrivateChannel(Customer customer, Customer destination, String name, String shortDesc) throws Exception {
        Log.l(TAG, "createNewPrivateChannel", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, "");
            statement.setString(2, "");
            statement.setLong(3, customer.getId());
            statement.setLong(4, new Date().getTime());
            statement.setLong(5, new Date().getTime());
            statement.setInt(6, ChannelType.PRIVATE.ordinal());
            statement.executeUpdate();

            rs = statement.getGeneratedKeys(); // will return the generated id
            rs.next();

            Log.l(TAG, "createNewPrivateChannel",
                    "New private channel added successfully name: " + name + " by: " + customer.getNickname());

            PrivateChannel privateChannel = (PrivateChannel) getChannelById(rs.getLong(1));

            connection.commit();

            Log.l(TAG, "createNewPrivateChannel",
                    "Adding new channel customer relation for the creator and for the destination");

            FactoryDao.getChannelMessageDao().addNewControlMessage(privateChannel, false, customer,
                    "<span class=\"user-link\" customerId=\"" + customer.getId() + "\">" +
                            customer.getNickname() + "</span> " + "created a new private channel");

            FactoryDao.getChannelCustomerRelationDao().addPrivateChannelCustomerRelation(customer, destination, privateChannel);

            return getAllCustomerChannelsByType(customer, ChannelType.PRIVATE);
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "createNewPrivateChannel", "Finished" + t.close());
        }
    }

    @Override
    public Channel getChannelById(Long channelId) throws Exception {
        Log.l(TAG, "getChannelById", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_BY_ID);
            statement.setLong(1, channelId);

            rs = statement.executeQuery();
            Channel channel = null;
            if (rs.next()) {
                ChannelType type = ChannelType.values()[rs.getInt("type")];

                if (type == ChannelType.PRIVATE) {
                    channel = new PrivateChannel(
                            rs.getLong("id"),
                            rs.getString("name"),
                            rs.getString("shortDesc"),
                            rs.getLong("createdByCustomerId"),
                            rs.getLong("createdAt"),
                            rs.getLong("updatedAt")
                    );
                } else {
                    channel = new PublicChannel(
                            rs.getLong("id"),
                            rs.getString("name"),
                            rs.getString("shortDesc"),
                            rs.getLong("createdByCustomerId"),
                            rs.getLong("createdAt"),
                            rs.getLong("updatedAt")
                    );
                }
            }

            if (channel == null) {
                throw new ChannelNotFound();
            }
            List<ChannelCustomerRelation> relations = FactoryDao.getChannelCustomerRelationDao().getChannelCustomerRelationByChannelId(channel.getId());
            List<Long> subscribers = new ArrayList<>();
            for (ChannelCustomerRelation relation : relations) {
                subscribers.add(relation.getCustomerId());
            }
            channel.setSubscribers(subscribers);

            Log.l(TAG, "getChannelById", "Channel found of id: " + channelId);

            return channel;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getChannelById", "Finished" + t.close());
        }
    }

    @Override
    public List<Channel> searchPublicChannels(String searchText) throws Exception {
        Log.l(TAG, "searchPublicChannels", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        List<Channel> channels = new ArrayList<>();
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.SEARCH_PUBLIC_CHANNEL_WHERE_NAME.replace("LIKE_HOLDER", searchText));
            rs = statement.executeQuery();
            while (rs.next()) {
                channels.add(new PublicChannel(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("shortDesc"),
                        rs.getLong("createdByCustomerId"),
                        rs.getLong("createdAt"),
                        rs.getLong("updatedAt")
                ));
            }
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "searchPublicChannels", "Finished" + t.close());
        }
        return channels;
    }

}
