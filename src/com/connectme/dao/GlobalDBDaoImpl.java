package com.connectme.dao;

import com.connectme.ConnectMeApp;
import com.connectme.enums.ChannelType;
import com.connectme.model.ChannelCustomerRelation;
import com.connectme.model.Customer;
import com.connectme.model.PublicChannel;
import com.connectme.util.Consts;
import com.connectme.util.Log;
import com.connectme.util.SqlStmt;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.ServletContextEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static com.connectme.util.SqlStmt.*;
import static com.connectme.util.SqlTable.*;

public class GlobalDBDaoImpl implements GlobalDBDao {

    private static final String TAG = GlobalDBDaoImpl.class.getSimpleName();

    @Override
    public void createTableIfNotExists(String tableStmt, String tableName) throws Exception {
        Log.l(TAG, "createTableIfNotExists", "Creating table \"" + tableName + "\"");

        Connection connection = null;
        Statement statement = null;
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.createStatement();
            statement.executeUpdate(tableStmt, Statement.RETURN_GENERATED_KEYS);
            connection.commit();
            Log.l(TAG, "createTableIfNotExists", "Table \"" + tableName + "\" created successfully!");
        } catch (SQLException e) {
            Log.e(TAG, "createTableIfNotExists", e.getMessage(), e);
            if (isTableAlreadyExists(e)) {
                Log.l(TAG, "createTableIfNotExists", "Table \"" + tableName + "\" already exists!");
            } else {
                throw e;
            }
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
        }
    }

    @Override
    public void dropTables() throws Exception {
        dropTable(CUSTOMERS_TABLE);
        dropTable(CHANNEL_CUSTOMER_RELATION_TABLE);
        dropTable(CHANNELS_TABLE);
        dropTable(CHANNEL_MESSAGE_TABLE);
    }

    @Override
    public void initTables(ServletContextEvent event) throws Exception {

        if (ConnectMeApp.DROP_DBS) {
            dropTables();
        }

        createTableIfNotExists(CREATE_CUSTOMERS_TABLE_STMT, CUSTOMERS_TABLE);
        createTableIfNotExists(CREATE_CUSTOMER_CHANNEL_RELATION_TABLE_STMT, CHANNEL_CUSTOMER_RELATION_TABLE);
        createTableIfNotExists(CREATE_CHANNELS_TABLE_STMT, CHANNELS_TABLE);
        createTableIfNotExists(CREATE_CHANNEL_MESSAGE_TABLE_STMT, CHANNEL_MESSAGE_TABLE);

        if (ConnectMeApp.DROP_DBS) {
            initDummyData(event);
        }
    }

    //utility that checks whether the customer tables already exists
    private boolean isTableAlreadyExists(SQLException e) {
        return e.getSQLState().equals("X0Y32");
    }

    private void dropTable(String tableName) {
        Log.l(TAG, "dropTable", "Dropping table \"" + tableName + "\"");
        Connection connection = null;
        Statement statement = null;
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.createStatement();
            statement.executeUpdate(String.format("DROP TABLE %s", tableName));
            connection.commit();
            Log.l(TAG, "dropTable", "Table \"" + tableName + "\" dropped successfully!");
        } catch (Exception ignore) {
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
        }
    }


    private void initDummyData(ServletContextEvent event) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CUSTOMER);

            List<Customer> customers = loadDummyCustomers(event);
            customers.add(new Customer(null, "david", "david", "David", "WOOHOO", "", new Date().getTime(),
                    new Date().getTime(), "WOOO", "WOOOOO"));
            for (Customer customer : customers) {
                Log.l(TAG, "initDummyData", customer.toString());
                statement.setString(1, customer.getUsername());
                statement.setString(2, customer.getPassword());
                statement.setString(3, customer.getNickname());
                statement.setString(4, customer.getShortDesc());
                statement.setString(5, customer.getPhoto());
                statement.setLong(6, new Date().getTime());
                statement.setLong(7, new Date().getTime());
                statement.setString(8, customer.getSecurityQuestion());
                statement.setString(9, customer.getSecurityAnswer());
                statement.executeUpdate();
                connection.commit();
            }

            Long publicChannelsIds = 1L;
            for (PublicChannel publicChannel : loadDummyChannels(event)) {
                statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL);

                Long customerId = (long) (new Random().nextInt(customers.size() - 10) + 10);
                Long customerId2 = (long) (new Random().nextInt(customers.size() - 10) + 10);
                Long customerId3 = (long) (new Random().nextInt(customers.size() - 10) + 10);
                int rand = new Random().nextInt(3);

                statement.setString(1, publicChannel.getName());
                statement.setString(2, publicChannel.getShortDesc());
                statement.setLong(3, customerId);
                statement.setLong(4, new Date().getTime());
                statement.setLong(5, new Date().getTime());
                statement.setInt(6, ChannelType.PUBLIC.ordinal());
                statement.executeUpdate();
                connection.commit();

                Log.l(TAG, "initDummyData", publicChannel.toString());

                ChannelCustomerRelation relation = new ChannelCustomerRelation(null, customerId, publicChannelsIds, new Date().getTime());
                statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_CUSTOMER_RELATION);
                statement.setLong(1, customerId);
                statement.setLong(2, publicChannelsIds);
                statement.setLong(3, new Date().getTime());
                statement.executeUpdate();
                connection.commit();
                Log.l(TAG, "initDummyData", relation.toString());

                if (rand == 1 && !Objects.equals(customerId2, customerId)) {
                    relation = new ChannelCustomerRelation(null, customerId2, publicChannelsIds, new Date().getTime());
                    statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_CUSTOMER_RELATION);
                    statement.setLong(1, customerId2);
                    statement.setLong(2, publicChannelsIds);
                    statement.setLong(3, new Date().getTime());
                    statement.executeUpdate();
                    connection.commit();
                    Log.l(TAG, "initDummyData", relation.toString());
                }

                if (!Objects.equals(customerId3, customerId) && !Objects.equals(customerId3, customerId2)) {
                    relation = new ChannelCustomerRelation(null, customerId3, publicChannelsIds, new Date().getTime());
                    statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_CUSTOMER_RELATION);
                    statement.setLong(1, customerId3);
                    statement.setLong(2, publicChannelsIds);
                    statement.setLong(3, new Date().getTime());
                    statement.executeUpdate();
                    connection.commit();
                    Log.l(TAG, "initDummyData", relation.toString());
                }

                publicChannelsIds++;
            }

        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
        }
    }

    private List<Customer> loadDummyCustomers(ServletContextEvent event) throws IOException {

        try (InputStream is = event.getServletContext().getResourceAsStream(Consts.DUMMY_CUSTOMERS_DATA_FILE)) {
            Type type = new TypeToken<List<Customer>>() {
            }.getType();
            return new Gson().fromJson(readStream(is), type);
        } catch (Exception e) {
            Log.e(TAG, "loadDummyCustomers", e.getLocalizedMessage(), e);
        }
        return new ArrayList<>();
    }

    private List<PublicChannel> loadDummyChannels(ServletContextEvent event) throws IOException {

        try (InputStream is = event.getServletContext().getResourceAsStream(Consts.DUMMY_PUBLIC_CHANNELS_DATA_FILE)) {
            Type type = new TypeToken<List<PublicChannel>>() {
            }.getType();
            return new Gson().fromJson(readStream(is), type);
        } catch (Exception e) {
            Log.e(TAG, "loadDummyChannels", e.getLocalizedMessage(), e);
        }
        return new ArrayList<>();
    }

    private String readStream(InputStream is) throws Exception {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            StringBuilder jsonFileContent = new StringBuilder();
            String nextLine;
            while ((nextLine = br.readLine()) != null) {
                jsonFileContent.append(nextLine);
            }
            return jsonFileContent.toString();
        }
    }
}
