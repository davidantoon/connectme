package com.connectme.dao;

import com.connectme.ConnectMeApp;
import com.connectme.exception.*;
import com.connectme.model.Customer;
import com.connectme.requests.ChangePasswordRequest;
import com.connectme.requests.ForgetPasswordRequest;
import com.connectme.requests.LoginRequest;
import com.connectme.requests.SignUpRequest;
import com.connectme.util.Log;
import com.connectme.util.OT;
import com.connectme.util.SqlStmt;
import com.connectme.util.ValidateInput;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomerDaoImpl implements CustomerDao {

    private static final String TAG = CustomerDaoImpl.class.getSimpleName();

    @Override
    public List<Customer> getAllCustomers(Boolean hideSensitiveData) throws Exception {
        Log.l(TAG, "getAllCustomers", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            List<Customer> customers = new ArrayList<>();
            statement = connection.prepareStatement(SqlStmt.GET_ALL_CUSTOMERS);
            rs = statement.executeQuery();
            while (rs.next()) {
                customers.add(new Customer(
                        rs.getLong("id"),
                        rs.getString("username"),
                        "", // Password empty for security reasons
                        rs.getString("nickname"),
                        rs.getString("shortdesc"),
                        rs.getString("photo"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedAt"),
                        hideSensitiveData ? "" : rs.getString("securityquestion"),
                        hideSensitiveData ? "" : rs.getString("securityanswer")
                ));
            }

            Log.l(TAG, "getAllCustomers", "Number of retrieved customers: " + customers.size());
            return customers;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getAllCustomers", "Finished" + t.close());
        }
    }

    @Override
    public Customer getCustomerById(Long customerId, Boolean hideSensitiveData) throws Exception {
        Log.l(TAG, "getCustomerById", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CUSTOMER_WHERE_CUSTOMER_ID);
            statement.setLong(1, customerId);
            rs = statement.executeQuery();
            if (rs.next()) {
                Log.l(TAG, "getCustomerById", "Customer found with id: " + customerId);
                return new Customer(
                        rs.getLong("id"),
                        rs.getString("username"),
                        "", // Password empty for security reasons
                        rs.getString("nickname"),
                        rs.getString("shortdesc"),
                        rs.getString("photo"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedAt"),
                        hideSensitiveData ? "" : rs.getString("securityquestion"),
                        hideSensitiveData ? "" : rs.getString("securityanswer")
                );
            }
            throw new CustomerNotFound();
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getCustomerById", "Finished" + t.close());
        }
    }

    @Override
    public Customer checkCredential(LoginRequest loginRequest) throws Exception {
        Log.l(TAG, "checkCredential", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.GET_CUSTOMER_WHERE_USERNAME_AND_PASSWORD);
            statement.setString(1, loginRequest.username);
            statement.setString(2, loginRequest.password);
            rs = statement.executeQuery();
            if (rs.next()) {
                Log.l(TAG, "checkCredential", "Logged in successfully username: " + loginRequest.username);
                return new Customer(
                        rs.getLong("id"),
                        rs.getString("username"),
                        "", // Password empty for security reasons
                        rs.getString("nickname"),
                        rs.getString("shortdesc"),
                        rs.getString("photo"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedAt"),
                        rs.getString("securityquestion"),
                        rs.getString("securityanswer")
                );
            }
            throw new UsernameOrPasswordIsIncorrect();
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "checkCredential", "Finished" + t.close());
        }
    }

    @Override
    public Customer addNewCustomer(SignUpRequest signUpRequest) throws Exception {
        Log.l(TAG, "addNewCustomer", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            if (!ValidateInput.isValidUsername(signUpRequest.username)) {
                throw new InvalidUsernameInput();
            }
            if (!ValidateInput.isValidPassword(signUpRequest.password)) {
                throw new InvalidPasswordInput();
            }
            if (!ValidateInput.isInRange(signUpRequest.nickname, 1, 20)) {
                throw new InvalidNicknameInput();
            }
            if (!ValidateInput.isInRange(signUpRequest.shortDesc, 0, 50)) {
                throw new InvalidShortDescInput();
            }
            if (!ValidateInput.isNotNullOrEmpty(signUpRequest.securityQuestion)) {
                throw new InvalidSecurityQuestionInput();
            }
            if (!ValidateInput.isNotNullOrEmpty(signUpRequest.securityAnswer)) {
                throw new InvalidSecurityAnswerInput();
            }

            try {
                // Check if username already exists
                getCustomerByUsername(signUpRequest.username, true);
                throw new UsernameAlreadyExists();
            } catch (CustomerNotFound e) {
                // If getCustomerByUsername throws CustomerNotFound Exception then we can add new customer
                // with this username
            }

            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CUSTOMER);
            statement.setString(1, signUpRequest.username);
            statement.setString(2, signUpRequest.password);
            statement.setString(3, signUpRequest.nickname);
            statement.setString(4, signUpRequest.shortDesc);
            statement.setString(5, signUpRequest.photo);
            statement.setLong(6, (new Date()).getTime());
            statement.setLong(7, (new Date()).getTime());
            statement.setString(8, signUpRequest.securityQuestion);
            statement.setString(9, signUpRequest.securityAnswer);
            statement.executeUpdate();

            connection.commit();
            Log.l(TAG, "addNewCustomer", "Customer has been added successfully " + signUpRequest.toString());
            return getCustomerByUsername(signUpRequest.username, false);
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
            Log.l(TAG, "addNewCustomer", "Finished" + t.close());
        }
    }

    @Override
    public Customer getCustomerByUsername(String username, Boolean hideSensitiveData) throws Exception {
        Log.l(TAG, "getCustomerByUsername", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.GET_CUSTOMER_WHERE_USERNAME);
            statement.setString(1, username);
            rs = statement.executeQuery();
            if (rs.next()) {
                Log.l(TAG, "getCustomerByUsername", "Customer found with username: " + username);
                return new Customer(
                        rs.getLong("id"),
                        rs.getString("username"),
                        "", // Password empty for security reasons
                        rs.getString("nickname"),
                        rs.getString("shortdesc"),
                        rs.getString("photo"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedAt"),
                        rs.getString("securityquestion"),
                        rs.getString("securityanswer")
                );
            }
            throw new CustomerNotFound();
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getCustomerByUsername", "Finished" + t.close());
        }
    }

    @Override
    public void changePassword(ChangePasswordRequest changePasswordRequest, Long customerId) throws Exception {
        Log.l(TAG, "changePassword", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.UPDATE_CUSTOMER_PASSWORD_WHERE_OLD_PASSWORD_AND_CUSTOMER_ID);
            statement.setString(1, changePasswordRequest.newPassword);
            statement.setLong(2, new Date().getTime());
            statement.setLong(3, customerId);
            statement.setString(4, changePasswordRequest.oldPassword);

            // If executeUpdate return > 0 that's mean an record has been updated in the database => old password correct
            if (statement.executeUpdate() == 0) {
                Log.l(TAG, "changePassword", "Incorrect old password for customerId: " + customerId);
                throw new IncorrectOldPasswordInput();
            }
            connection.commit();
            Log.l(TAG, "changePassword", "New password assigned for customerId: " + customerId);
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
            Log.l(TAG, "changePassword", "Finished" + t.close());
        }
    }

    @Override
    public void forgetPassword(ForgetPasswordRequest forgetPasswordRequest) throws Exception {
        Log.l(TAG, "forgetPassword", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            if (!ValidateInput.isValidUsername(forgetPasswordRequest.username)) {
                throw new InvalidUsernameInput();
            }
            if (!ValidateInput.isValidPassword(forgetPasswordRequest.newPassword)) {
                throw new InvalidPasswordInput();
            }
            if (!ValidateInput.isNotNullOrEmpty(forgetPasswordRequest.securityQuestion)) {
                throw new InvalidSecurityQuestionInput();
            }
            if (!ValidateInput.isNotNullOrEmpty(forgetPasswordRequest.securityAnswer)) {
                throw new InvalidSecurityAnswerInput();
            }

            // Check if the customer exists with passed username
            getCustomerByUsername(forgetPasswordRequest.username, false);

            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.UPDATE_CUSTOMER_PASSWORD_WHERE_USERNAME_SECURITY_Q_A);
            statement.setString(1, forgetPasswordRequest.newPassword);
            statement.setLong(2, new Date().getTime());
            statement.setString(3, forgetPasswordRequest.username);
            statement.setString(4, forgetPasswordRequest.securityQuestion);
            statement.setString(5, forgetPasswordRequest.securityAnswer);

            // If executeUpdate return > 0 that's mean an record has been updated in the database => security info and username matches
            if (statement.executeUpdate() == 0) {
                Log.l(TAG, "forgetPassword", "Incorrect security info for username: " + forgetPasswordRequest.username);
                throw new IncorrectOldPasswordInput();
            }
            connection.commit();
            Log.l(TAG, "changePassword", "New password assigned for username: " + forgetPasswordRequest.username);

        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
            Log.l(TAG, "forgetPassword", "Finished" + t.close());
        }
    }

    @Override
    public Customer getCustomerByNickname(String nickname) throws Exception {
        Log.l(TAG, "getCustomerByNickname", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CUSTOMER_WHERE_CUSTOMER_NICKNAME);
            statement.setString(1, nickname);
            rs = statement.executeQuery();
            if (rs.next()) {
                return new Customer(
                        rs.getLong("id"),
                        rs.getString("username"),
                        "", // Password empty for security reasons
                        rs.getString("nickname"),
                        rs.getString("shortdesc"),
                        rs.getString("photo"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedAt"),
                        "", ""
                );
            }
            throw new CustomerNotFound();
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getCustomerByNickname", "Finished" + t.close());
        }
    }

    @Override
    public List<Customer> searchCustomers(String searchText) throws Exception {
    	Log.l(TAG, "searchCustomers", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        List<Customer> customers = new ArrayList<>(); 
        try {
            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.SEARCH_CUSTOMER_WHERE_NICKNAME.replace("LIKE_HOLDER", searchText));
            rs = statement.executeQuery();
            while (rs.next()) {
            	customers.add(new Customer(
                        rs.getLong("id"),
                        "",
                        "", // Password empty for security reasons
                        rs.getString("nickname"),
                        rs.getString("shortdesc"),
                        rs.getString("photo"),
                        0L,0L,"", ""
                ));
            }
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "searchCustomers", "Finished" + t.close());
        }
        return customers;
    }
}
