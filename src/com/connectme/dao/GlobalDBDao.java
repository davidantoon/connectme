package com.connectme.dao;

import javax.servlet.ServletContextEvent;
import java.sql.SQLException;

public interface GlobalDBDao {

    void createTableIfNotExists(String tableStmt, String tableName) throws Exception;

    void dropTables() throws Exception;

    void initTables(ServletContextEvent event) throws Exception;
}
