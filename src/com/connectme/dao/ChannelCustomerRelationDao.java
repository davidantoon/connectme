package com.connectme.dao;

import com.connectme.exception.ChannelCustomerRelationAlreadyExists;
import com.connectme.exception.ChannelCustomerRelationNotFound;
import com.connectme.model.*;

import java.util.List;

public interface ChannelCustomerRelationDao {

    /**
     * Get specific channel <-> customer relation object by relation id
     *
     * @param relationId the id of the channel relation
     * @return {@link ChannelCustomerRelation} if exists
     * @throws ChannelCustomerRelationNotFound if not found
     */
    ChannelCustomerRelation getChannelCustomerRelationById(Long relationId) throws Exception;

    /**
     * Get specific channel <-> customer relation object by channel id and customer id
     *
     * @param channel  the channel object
     * @param customer the customer object
     * @return {@link ChannelCustomerRelation} if exists
     * @throws ChannelCustomerRelationNotFound if not found
     */
    ChannelCustomerRelation getChannelCustomerRelationByChannelIdAndCustomerId(Channel channel, Customer customer) throws Exception;

    /**
     * Get all channel <-> customer relation object by channel id
     *
     * @param channelId the id of the channel
     * @return {@link List<ChannelCustomerRelation>} for all channel id
     */
    List<ChannelCustomerRelation> getChannelCustomerRelationByChannelId(Long channelId) throws Exception;

    /**
     * Add a new {@link ChannelCustomerRelation} to subscribe customer to the public channel
     *
     * @param customer      customer to be subscribed
     * @param publicChannel the public channel to subscribe to
     * @return the inserted {@link ChannelCustomerRelation} object
     * @throws ChannelCustomerRelationAlreadyExists if exists
     */
    ChannelCustomerRelation addCustomerToPublicChannel(Customer customer, PublicChannel publicChannel) throws Exception;

    /**
     * Remove existing relation to unsubscribe user from specific public channel
     *
     * @param customer      the customer to unsubscribe it
     * @param publicChannel the channel to unsubscribe from
     * @throws ChannelCustomerRelationNotFound if relation not found
     */
    void removeCustomerFromPublicChannel(Customer customer, PublicChannel publicChannel) throws Exception;

    /**
     * Add a new {@link ChannelCustomerRelation} to subscribe two customers in specific private channel
     *
     * @param customer1      customer who creates the private channel
     * @param customer2      the destination customer in the private channel
     * @param privateChannel private channel to subscribe to
     * @throws ChannelCustomerRelationAlreadyExists if exists
     */
    void addPrivateChannelCustomerRelation(Customer customer1, Customer customer2, PrivateChannel privateChannel) throws Exception;

    /**
     * Get all customer subscribed channels included private and public
     * for socket initialization per customer
     *
     * @param customer the subscriber customer
     * @return {@link List<ChannelCustomerRelation>} all subscribed channels
     */
    List<ChannelCustomerRelation> getAllCustomerChannels(Customer customer) throws Exception;
}
