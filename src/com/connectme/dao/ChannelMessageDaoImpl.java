package com.connectme.dao;

import com.connectme.ConnectMeApp;
import com.connectme.enums.ChannelNotificationType;
import com.connectme.enums.MessageType;
import com.connectme.exception.*;
import com.connectme.model.*;
import com.connectme.util.Log;
import com.connectme.util.OT;
import com.connectme.util.SqlStmt;
import com.connectme.util.ValidateInput;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChannelMessageDaoImpl implements ChannelMessageDao {


    private static final String TAG = ChannelMessageDaoImpl.class.getSimpleName();

    @Override
    public List<ChannelMessage> getMassageByChannelAndOffset(Customer customer, Long channelId, Long offset) throws Exception {
        Log.l(TAG, "getMassageByChannelAndOffset", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            connection = ConnectMeApp.getDBConnection();
            Channel channel = FactoryDao.getChannelDao().getChannelById(channelId);

            if (channel == null) {
                throw new ChannelNotFound();
            }

            ChannelCustomerRelation channelCustomerRelation = FactoryDao.getChannelCustomerRelationDao()
                    .getChannelCustomerRelationByChannelIdAndCustomerId(channel, customer);

            if (channelCustomerRelation == null) {
                throw new CustomerNotSubscribedToChannel();
            }

            List<Customer> customers = FactoryDao.getCustomerDao().getAllCustomers(true);
            HashMap<Long, Customer> hashMapCus = new HashMap<>();
            for (Customer c : customers) {
                hashMapCus.put(c.getId(), c);
            }

            List<ChannelMessage> messages = new ArrayList<>();
            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_MESSAGES);
            statement.setLong(1, channelId);
            statement.setLong(2, offset);
            rs = statement.executeQuery();
            while (rs.next()) {
                ChannelMessage channelMessage = new ChannelMessage(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getString("message"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedat"),
                        rs.getString("readby"),
                        rs.getString("mentions"),
                        MessageType.values()[rs.getInt("type")],
                        rs.getLong("parentid"),
                        rs.getLong("channelid")
                );
                channelMessage.setCustomer(hashMapCus.get(channelMessage.getCustomerId()));
                messages.add(channelMessage);
            }
            statement.close();

            Log.l(TAG, "getMassageByChannelAndOffset", "Found channel messages: " + messages.size());

            return messages;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getMassageByChannelAndOffset", "Finished" + t.close());
        }
    }

    @Override
    public void readAllMessagesInChannel(Customer customer, Long channelId) throws Exception {
        Log.l(TAG, "readAllMessagesInChannel", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            connection = ConnectMeApp.getDBConnection();
            Channel channel = FactoryDao.getChannelDao().getChannelById(channelId);

            if (channel == null) {
                throw new ChannelNotFound();
            }

            ChannelCustomerRelation channelCustomerRelation = FactoryDao.getChannelCustomerRelationDao()
                    .getChannelCustomerRelationByChannelIdAndCustomerId(channel, customer);

            if (channelCustomerRelation == null) {
                throw new CustomerNotSubscribedToChannel();
            }

            List<ChannelMessage> messages = new ArrayList<>();
            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_UNREAD_MESSAGES.replace("LIKE_HOLDER", customer.getId() + ""));
            statement.setLong(1, channelId);
            statement.setLong(2, channelCustomerRelation.getCreatedAt());
            rs = statement.executeQuery();
            while (rs.next()) {
                messages.add(new ChannelMessage(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getString("message"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedat"),
                        rs.getString("readby"),
                        rs.getString("mentions"),
                        MessageType.values()[rs.getInt("type")],
                        rs.getLong("parentid"),
                        rs.getLong("channelid")
                ));
            }
            statement.close();

            Log.l(TAG, "readAllMessagesInChannel", "Found unread messages: " + messages.size());

            int markedAsUnread = 0;
            for (ChannelMessage message : messages) {
                if (!message.isReadByCustomer(customer.getId())) {
                    String readby = message.getReadBy();
                    readby = readby.isEmpty() ? (customer.getId() + "") : (readby + "," + customer.getId());
                    statement = connection.prepareStatement(SqlStmt.MARK_MESSAGE_AS_READ);
                    statement.setString(1, readby);
                    statement.setLong(2, message.getId());
                    statement.executeUpdate();
                    statement.close();
                    markedAsUnread++;
                }
            }

            Log.l(TAG, "readAllMessagesInChannel", "Marked " + markedAsUnread + " messages as read");

        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "readAllMessagesInChannel", "Finished" + t.close());
        }
    }

    @Override
    public ChannelMessageMeta getUnreadMessagesForCustomerByChannel(Customer customer, Long channelId) throws Exception {
        Log.l(TAG, "getUnreadMessagesForCustomerByChannel", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            connection = ConnectMeApp.getDBConnection();
            Channel channel = FactoryDao.getChannelDao().getChannelById(channelId);
            if (channel == null) {
                throw new ChannelNotFound();
            }

            ChannelCustomerRelation channelCustomerRelation = FactoryDao.getChannelCustomerRelationDao()
                    .getChannelCustomerRelationByChannelIdAndCustomerId(channel, customer);

            if (channelCustomerRelation == null) {
                throw new CustomerNotSubscribedToChannel();
            }
            List<ChannelMessage> messages = new ArrayList<>();
            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_UNREAD_MESSAGES.replace("LIKE_HOLDER", customer.getId() + ""));
            statement.setLong(1, channelId);
            statement.setLong(2, channelCustomerRelation.getCreatedAt());
            rs = statement.executeQuery();
            while (rs.next()) {
                messages.add(new ChannelMessage(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getString("message"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedat"),
                        rs.getString("readby"),
                        rs.getString("mentions"),
                        MessageType.values()[rs.getInt("type")],
                        rs.getLong("parentid"),
                        rs.getLong("channelid")
                ));
            }

            Boolean isMentioned = false;
            for (ChannelMessage channelMessage : messages) {
                if (("," + channelMessage.getMentions() + ",").contains("," + customer.getId() + ",")) {
                    isMentioned = true;
                    break;
                }
            }

            List<ChannelMessage> first10 = getMassageByChannelAndOffset(customer, channelId, new Date().getTime());
            if (first10.size() > 0) {
                if (("," + first10.get(0).getMentions() + ",").contains("," + customer.getId() + ",")) {
                    isMentioned = true;
                }
            }
            return new ChannelMessageMeta(
                    messages.size(),
                    first10.size() == 0 ? null : first10.get(0),
                    isMentioned
            );
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getUnreadMessagesForCustomerByChannel", "Finished" + t.close());
        }
    }

    @Override
    public ChannelMessage addNewMessage(Channel channel, Customer customer, String message) throws Exception {

        Log.l(TAG, "addNewMessage", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            if (!ValidateInput.isInRange(message, 1, 500)) {
                throw new MessageMustBeNotEmptyAndLessThan500();
            }
            connection = ConnectMeApp.getDBConnection();

            // Only apply mention on subscribed customers
            List<ChannelCustomerRelation> subscribedCustomers = FactoryDao.getChannelCustomerRelationDao()
                    .getChannelCustomerRelationByChannelId(channel.getId());
            String mentionsStr = getMentions(message, subscribedCustomers);
            message = refactorMentions(message, subscribedCustomers);

            ChannelMessage channelMessage = new ChannelMessage(null, customer.getId(),
                    message, new Date().getTime(), new Date().getTime(), customer.getId() + "", mentionsStr,
                    MessageType.REGULAR, 0L, channel.getId());

            Log.l(TAG, "addNewMessage", channelMessage.toString());
            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_MESSAGE, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, customer.getId());
            statement.setString(2, message);
            statement.setLong(3, channelMessage.getCreatedAt());
            statement.setLong(4, channelMessage.getUpdatedAt());
            statement.setLong(5, customer.getId());
            statement.setString(6, mentionsStr);
            statement.setLong(7, MessageType.REGULAR.ordinal());
            statement.setLong(8, 0);
            statement.setLong(9, channel.getId());
            statement.executeUpdate();

            rs = statement.getGeneratedKeys(); // will return the generated id
            rs.next();
            channelMessage.setId(rs.getLong(1));

            Log.l(TAG, "addNewMessage", "New message added successfully by: " + customer.getNickname());

            channelMessage.setCustomer(customer);

            ConnectMeApp.getChannelSubscription().notifyChannel(channel, customer, channelMessage, ChannelNotificationType.NEW_CHANNEL_MESSAGE);
            return channelMessage;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "addNewMessage", "Finished" + t.close());
        }
    }


    @Override
    public ChannelMessage addNewControlMessage(Channel channel, boolean notify, Customer customer, String message) throws Exception {
        Log.l(TAG, "addNewControlMessage", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            connection = ConnectMeApp.getDBConnection();

            // Only apply mention on subscribed customers
            List<ChannelCustomerRelation> subscribedCustomers = FactoryDao.getChannelCustomerRelationDao()
                    .getChannelCustomerRelationByChannelId(channel.getId());

            ChannelMessage channelMessage = new ChannelMessage(null, customer.getId(),
                    message, new Date().getTime(), new Date().getTime(), customer.getId() + "", getMentions(message, subscribedCustomers),
                    MessageType.CONTROL, 0L, channel.getId());

            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_MESSAGE, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, customer.getId());
            statement.setString(2, message);
            statement.setLong(3, new Date().getTime());
            statement.setLong(4, new Date().getTime());
            statement.setLong(5, customer.getId());
            statement.setString(6, "");
            statement.setLong(7, MessageType.CONTROL.ordinal());
            statement.setLong(8, 0);
            statement.setLong(9, channel.getId());
            statement.executeUpdate();

            rs = statement.getGeneratedKeys(); // will return the generated id
            rs.next();
            channelMessage.setId(rs.getLong(1));

            connection.commit();

            Log.l(TAG, "addNewControlMessage", "New control message added successfully by: " + customer.getNickname());

            ConnectMeApp.getChannelSubscription().notifyChannel(channel, customer, channelMessage, ChannelNotificationType.NEW_CHANNEL_MESSAGE);

            channelMessage.setCustomer(customer);
            return channelMessage;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "addNewControlMessage", "Finished" + t.close());
        }
    }

    @Override
    public ChannelMessage getMessageById(Long id) throws Exception {
        Log.l(TAG, "getMessageById", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_MESSAGE_BY_ID);
            statement.setLong(1, id);

            rs = statement.executeQuery();
            if (rs.next()) {
                return new ChannelMessage(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getString("message"),
                        rs.getLong("createdat"),
                        rs.getLong("updatedat"),
                        rs.getString("readby"),
                        rs.getString("mentions"),
                        MessageType.values()[rs.getInt("type")],
                        rs.getLong("parentid"),
                        rs.getLong("channelid")
                );
            }
            throw new ChannelMessageNotFound();
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getMassageByChannelAndOffset", "Finished" + t.close());
        }
    }

    private String getMentions(String message, List<ChannelCustomerRelation> customerRelations) {
        List<String> mentions = new ArrayList<>();
        Pattern p = Pattern.compile("@[0-9a-zA-Z-_]+");
        Matcher m = p.matcher(message);
        while (m.find()) {
            if (!mentions.contains(m.group())) {
                mentions.add(m.group());
            }
        }

        String mentionsStr = "";
        for (String user : mentions) {
            try {
                Customer customer = null;
                for (ChannelCustomerRelation relation : customerRelations) {
                    Customer temp = FactoryDao.getCustomerDao().getCustomerById(relation.getCustomerId(), true);
                    if (temp.getNickname().equals(user.substring(1))) {
                        customer = temp;
                        break;
                    }
                }
                if (customer == null)
                    throw new CustomerNotFound();

                mentionsStr += customer.getId() + ",";
            } catch (Exception e) {
                Log.e(TAG, "getMentions", e.getLocalizedMessage(), e);
            }
        }
        return mentionsStr.length() > 0 ? mentionsStr.substring(0, mentionsStr.length() - 1) : "";
    }

    private String refactorMentions(String message, List<ChannelCustomerRelation> customerRelations) {
        List<String> mentions = new ArrayList<>();
        Pattern p = Pattern.compile("@[0-9a-zA-Z-_]+");
        Matcher m = p.matcher(message);
        while (m.find()) {
            if (!mentions.contains(m.group())) {
                mentions.add(m.group());
            }
        }

        String newMessage = message;
        for (String user : mentions) {
            try {
                Customer customer = null;
                for (ChannelCustomerRelation relation : customerRelations) {
                    Customer temp = FactoryDao.getCustomerDao().getCustomerById(relation.getCustomerId(), true);
                    if (temp.getNickname().equals(user.substring(1))) {
                        customer = temp;
                        break;
                    }
                }
                if (customer == null)
                    throw new CustomerNotFound();

                newMessage = newMessage.replaceAll(user + "+", "<span class=\"user-link\" customerId=\"" + customer.getId() + "\">" +
                        customer.getNickname() + "</span> ");

            } catch (Exception e) {
                Log.e(TAG, "getMentions", e.getLocalizedMessage(), e);
            }
        }
        return newMessage;
    }

}
