package com.connectme.dao;

import com.connectme.requests.LoginRequest;

public class FactoryDao {

    private static ChannelDao channelDaoInstance = null;
    private static ChannelCustomerRelationDao channelCustomerRelationDaoInstance = null;
    private static ChannelMessageDao channelMessageDaoInstance = null;
    private static CustomerDao customerDaoInstance = null;
    private static GlobalDBDaoImpl globalDBDaoInstance = null;

    public static ChannelDao getChannelDao() {
        if (channelDaoInstance == null)
            channelDaoInstance = new ChannelDaoImpl();
        return channelDaoInstance;
    }

    public static ChannelCustomerRelationDao getChannelCustomerRelationDao() {
        if (channelCustomerRelationDaoInstance == null)
            channelCustomerRelationDaoInstance = new ChannelCustomerRelationDaoImpl();
        return channelCustomerRelationDaoInstance;
    }

    public static ChannelMessageDao getChannelMessageDao() {
        if (channelMessageDaoInstance == null)
            channelMessageDaoInstance = new ChannelMessageDaoImpl();
        return channelMessageDaoInstance;
    }

    public static CustomerDao getCustomerDao() {
        if (customerDaoInstance == null)
            customerDaoInstance = new CustomerDaoImpl();
        return customerDaoInstance;
    }

    public static GlobalDBDao getGlobalDBDao() {
        if (globalDBDaoInstance == null)
            globalDBDaoInstance = new GlobalDBDaoImpl();
        return globalDBDaoInstance;
    }

}
