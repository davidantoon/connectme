package com.connectme.dao;

import com.connectme.enums.ChannelType;
import com.connectme.exception.ChannelNotFound;
import com.connectme.model.*;

import java.util.List;

public interface ChannelDao {

    /**
     * Get all channels for passed customer and specific {@link ChannelType}
     *
     * @param customer the channel subscriber
     * @param type     type of the channels
     * @return {@link List<Channel> } by type
     */
    List<Channel> getAllCustomerChannelsByType(Customer customer, ChannelType type) throws Exception;


    /**
     * Add new public channel to the database,
     * Create new {@link ChannelCustomerRelation} for the creator
     *
     * @param customer  channel creator
     * @param name      channel name
     * @param shortDesc channel short description
     * @return the updates {@link PublicChannel} list
     */
    List<Channel> createNewPublicChannel(Customer customer, String name, String shortDesc) throws Exception;

    /**
     * Add new private channel to the dataabse,
     * Create new {@link ChannelCustomerRelation} for both creator and destination customers,
     * Add new {@link ChannelMessage} with the message text "{@link Customer#nickname} added you to private channel"
     *
     * @param customer    channel creator
     * @param destination channel second subscriber
     * @param name        channel name
     * @param shortDesc   channel short description
     * @return the updates {@link PrivateChannel} list
     */
    List<Channel> createNewPrivateChannel(Customer customer, Customer destination, String name, String shortDesc) throws Exception;


    /**
     * Get channel object by channel id
     *
     * @param channelId the channelid to retrieve
     * @return {@link Channel} object if exists
     * @throws ChannelNotFound if not found
     */
    Channel getChannelById(Long channelId) throws Exception;

    /**
     * Search in public channels by channel name
     *
     * @param searchText the search text
     * @return {@link List<Channel>} matched the search text
     * @throws Exception 
     */
    List<Channel> searchPublicChannels(String searchText)  throws Exception;
}
