package com.connectme.dao;

import com.connectme.ConnectMeApp;
import com.connectme.enums.ChannelNotificationType;
import com.connectme.exception.ChannelCustomerRelationNotFound;
import com.connectme.exception.FailedToRemoveChannelCustomerRelation;
import com.connectme.exception.ChannelCustomerRelationAlreadyExists;
import com.connectme.model.*;
import com.connectme.util.Log;
import com.connectme.util.OT;
import com.connectme.util.SqlStmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChannelCustomerRelationDaoImpl implements ChannelCustomerRelationDao {

    private static final String TAG = ChannelCustomerRelationDaoImpl.class.getSimpleName();

    @Override
    public ChannelCustomerRelation getChannelCustomerRelationById(Long relationId) throws Exception {
        Log.l(TAG, "getChannelCustomerRelationById", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_CUSTOMER_RELATION_BY_ID);
            statement.setLong(1, relationId);

            rs = statement.executeQuery();
            if (rs.next()) {
                ChannelCustomerRelation relation = new ChannelCustomerRelation(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getLong("channelid"),
                        rs.getLong("createdat")
                );

                Log.l(TAG, "getChannelCustomerRelationById", "Channel customer relation found for id: " + relationId);
                return relation;
            }
            throw new ChannelCustomerRelationNotFound();
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getChannelCustomerRelationById", "Finished" + t.close());
        }
    }

    @Override
    public ChannelCustomerRelation getChannelCustomerRelationByChannelIdAndCustomerId(Channel channel, Customer customer) throws Exception {
        Log.l(TAG, "getChannelCustomerRelationByChannelIdAndCustomerId", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_CUSTOMER_RELATION_BY_CHANNEL_ID_AND_CUSTOMER_ID);
            statement.setLong(1, channel.getId());
            statement.setLong(2, customer.getId());

            rs = statement.executeQuery();
            if (rs.next()) {
                ChannelCustomerRelation relation = new ChannelCustomerRelation(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getLong("channelid"),
                        rs.getLong("createdat")
                );

                Log.l(TAG, "getChannelCustomerRelationByChannelIdAndCustomerId",
                        "Channel customer relation found for customer id: " + customer.getId() +
                                " and channel id: " + channel.getId());
                return relation;
            }
            throw new ChannelCustomerRelationNotFound();
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getChannelCustomerRelationByChannelIdAndCustomerId", "Finished" + t.close());
        }
    }

    @Override
    public List<ChannelCustomerRelation> getChannelCustomerRelationByChannelId(Long channelId) throws Exception {
        Log.l(TAG, "getChannelCustomerRelationByChannelId", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_CUSTOMER_RELATION_BY_CHANNEL_ID);
            statement.setLong(1, channelId);

            rs = statement.executeQuery();

            List<ChannelCustomerRelation> relations = new ArrayList<>();
            while (rs.next()) {
                relations.add(new ChannelCustomerRelation(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getLong("channelid"),
                        rs.getLong("createdat")
                ));
            }
            Log.l(TAG, "getChannelCustomerRelationByChannelId", "Channel customer relations count: " + relations.size());
            return relations;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getChannelCustomerRelationByChannelId", "Finished" + t.close());
        }
    }

    @Override
    public ChannelCustomerRelation addCustomerToPublicChannel(Customer customer, PublicChannel publicChannel) throws Exception {

        Log.l(TAG, "addCustomerToPublicChannel", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {

            try {
                getChannelCustomerRelationByChannelIdAndCustomerId(publicChannel, customer);
                // throw exception to prevent duplicate relation for specific customer and channel relation
                throw new ChannelCustomerRelationAlreadyExists();
            } catch (ChannelCustomerRelationNotFound e) {
                // this is VALID state that we don't have another relation
            }

            connection = ConnectMeApp.getDBConnection();
            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_CUSTOMER_RELATION, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, customer.getId());
            statement.setLong(2, publicChannel.getId());
            statement.setLong(3, new Date().getTime());
            statement.executeUpdate();


            rs = statement.getGeneratedKeys(); // will return the generated id
            rs.next();
            Log.l(TAG, "addCustomerToPublicChannel",
                    "Customer added to public channel successfully channelId: " + publicChannel.getId() +
                            " customerId: " + customer.getId());


            FactoryDao.getChannelMessageDao().addNewControlMessage(publicChannel, true, customer,
                    "<span class=\"user-link\" customerId=\"" + customer.getId() + "\">" +
                            customer.getNickname() + "</span> " + "subscribed to the channel");

            return getChannelCustomerRelationById(rs.getLong(1));
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "addCustomerToPublicChannel", "Finished" + t.close());
        }
    }

    @Override
    public void removeCustomerFromPublicChannel(Customer customer, PublicChannel publicChannel) throws Exception {
        Log.l(TAG, "removeCustomerFromPublicChannel", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.DELETE_CHANNEL_CUSTOMER_RELATION, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, customer.getId());
            statement.setLong(2, publicChannel.getId());

            // If executeUpdate return > 0 that's mean an record has been updated or deleted
            if (statement.executeUpdate() == 0) {
                Log.l(TAG, "removeCustomerFromPublicChannel", "Failed to remove channel customer relation " +
                        "customerId: " + customer.getId() + " channelId: " + publicChannel.getId());
                throw new FailedToRemoveChannelCustomerRelation();
            }
            connection.commit();
            Log.l(TAG, "removeCustomerFromPublicChannel", "Channel customer relation " +
                    "with customerId: " + customer.getId() + " channelId: " + publicChannel.getId() + " removed successfully");

            // fetch to update the latest subscribers list
            publicChannel = (PublicChannel) FactoryDao.getChannelDao().getChannelById(publicChannel.getId());

            FactoryDao.getChannelMessageDao().addNewControlMessage(publicChannel, true, customer,
                    "<span class=\"user-link\" customerId=\"" + customer.getId() + "\">" +
                            customer.getNickname() + "</span> " + "unsubscribed fomr the channel");

        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
            Log.l(TAG, "removeCustomerFromPublicChannel", "Finished" + t.close());
        }
    }

    @Override
    public void addPrivateChannelCustomerRelation(Customer customer1, Customer customer2,
                                                  PrivateChannel privateChannel) throws Exception {

        Log.l(TAG, "addPrivateChannelCustomerRelation", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            /*
              To prevent multiple private channel for the same two users, throw exception
              if there is existing private channel relation
             */
            if (getChannelCustomerRelationByChannelId(privateChannel.getId()).size() > 0) {
                Log.l(TAG, "addPrivateChannelCustomerRelation",
                        "Current private channel already have relations channelId: " + privateChannel.getId());
                throw new ChannelCustomerRelationAlreadyExists();
            }

            connection = ConnectMeApp.getDBConnection();


            Log.l(TAG, "addPrivateChannelCustomerRelation",
                    "Adding the first customer to private channel, customerId: " + customer1.getId() +
                            " channelId: " + privateChannel.getId());
            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_CUSTOMER_RELATION, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, customer1.getId());
            statement.setLong(2, privateChannel.getId());
            statement.setLong(3, new Date().getTime());
            statement.executeUpdate();


            Log.l(TAG, "addPrivateChannelCustomerRelation",
                    "Adding the second customer to private channel, customerId: " + customer2.getId() +
                            " channelId: " + privateChannel.getId());

            statement = connection.prepareStatement(SqlStmt.INSERT_NEW_CHANNEL_CUSTOMER_RELATION, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, customer2.getId());
            statement.setLong(2, privateChannel.getId());
            statement.setLong(3, new Date().getTime());
            statement.executeUpdate();

            connection.commit();

            Log.l(TAG, "addPrivateChannelCustomerRelation",
                    "New private channel customer relation added successfully, first customerId:" + customer1.getId() +
                            " second customerId:" + customer2.getId() + " channelId: " + privateChannel.getId());

            FactoryDao.getChannelMessageDao().addNewControlMessage(privateChannel, false, customer1,
                    "<span class=\"user-link\" customerId=\"" + customer1.getId() + "\">" +
                            customer1.getNickname() + "</span> " +
                            "added <span class=\"user-link\" customerId=\"" + customer2.getId() + "\">"
                            + customer2.getNickname() + "</span> to the channel");

            // notify only the other side of the private channel that has been added to this channel
            ConnectMeApp.getChannelSubscription().notifyCustomer(privateChannel, customer2, ChannelNotificationType.NEW_PRIVATE_CHANNEL);
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, null);
            Log.l(TAG, "addPrivateChannelCustomerRelation", "Finished" + t.close());
        }
    }


    @Override
    public List<ChannelCustomerRelation> getAllCustomerChannels(Customer customer) throws Exception {
        Log.l(TAG, "getAllCustomerChannels", "Called");
        OT t = new OT();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = ConnectMeApp.getDBConnection();

            statement = connection.prepareStatement(SqlStmt.GET_CHANNEL_CUSTOMER_RELATION_BY_CUSTOMER_ID);
            statement.setLong(1, customer.getId());

            rs = statement.executeQuery();

            List<ChannelCustomerRelation> relations = new ArrayList<>();
            while (rs.next()) {
                relations.add(new ChannelCustomerRelation(
                        rs.getLong("id"),
                        rs.getLong("customerid"),
                        rs.getLong("channelid"),
                        rs.getLong("createdat")
                ));
            }
            Log.l(TAG, "getAllCustomerChannels", "Channel for customerId:" + customer.getId() + " count: " + relations.size());
            return relations;
        } finally {
            ConnectMeApp.closeDBConnection(connection, statement, rs);
            Log.l(TAG, "getAllCustomerChannels", "Finished" + t.close());
        }
    }
}
