package com.connectme.servlet;

import com.connectme.dao.FactoryDao;
import com.connectme.exception.ConnectMeException;
import com.connectme.exception.CustomerNotFound;
import com.connectme.exception.UnsupportedFunction;
import com.connectme.exception.UsernameOrPasswordIsIncorrect;
import com.connectme.model.Customer;
import com.connectme.requests.*;
import com.connectme.util.HttpUtil;
import com.connectme.util.Log;
import com.connectme.util.OT;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(
        name = "CustomerServlet",
        description = "Customer servlet manage all customers operations",
        urlPatterns = {
                "/api/customers",
                "/api/customer/*",
                "/api/customer",
                "/api/customer/forget_password",
                "/api/customer/change_password",
                "/api/search/customer/*",
                "/api/login",
                "/login",
                "/api/logout",
        }
)
public class CustomerServlet extends HttpServlet {

    private static final long serialVersionUID = -34774005733232585L;
    private static final String TAG = CustomerServlet.class.getSimpleName();
    private static final String API_GET_ALL = "/api/customers";
    private static final String API_GET_BY_CUSTOMER_ID = "/api/customer/";
    private static final String API_GET_LOGGED_IN_CUSTOMER = "/api/customer";
    private static final String API_FORGET_PASSWORD = "/api/customer/forget_password";
    private static final String API_CHANGE_PASSWORD = "/api/customer/change_password";
    private static final String API_SIGN_UP = "/api/customer";
    private static final String API_LOGIN = "/api/login";
    private static final String LOGIN_FORM = "/login";
    private static final String API_LOGOUT = "/api/logout";
    private static final String SEARCH_CUSTOMERS = "/api/search/customer/";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OT ot = new OT();
        PrintWriter printWriter = resp.getWriter();
        Gson gson = new Gson();
        try {
            String uri = req.getRequestURI();
            resp.setContentType("application/json");
            Log.l(TAG, "goGet", String.format("GET Request URI: %s", uri));
            String pathInfo = req.getPathInfo();

            if (uri.contains(API_GET_ALL)) {
                HttpUtil.checkSession(req);
                printWriter.println(gson.toJson(FactoryDao.getCustomerDao().getAllCustomers(true)));
                return;

            } else if (uri.contains(API_GET_BY_CUSTOMER_ID)) {
                HttpUtil.checkSession(req);

                String[] pathParts = pathInfo.split("/");
                if (pathParts.length == 0) {
                    throw new CustomerNotFound();
                }
                String customerIdStr = pathParts[1];
                Long customerId;
                Log.l(TAG, "goGet", String.format("Get customer by id: %s", customerIdStr));
                try {
                    if (customerIdStr == null || customerIdStr.length() == 0) {
                        throw new CustomerNotFound();
                    }
                    customerId = Long.parseLong(customerIdStr);
                } catch (Exception e) {
                    Log.l(TAG, "doGet", "Invalid customer id");
                    throw new CustomerNotFound();
                }
                printWriter.println(gson.toJson(FactoryDao.getCustomerDao().getCustomerById(customerId, true)));
                return;

            } else if (uri.contains(API_GET_LOGGED_IN_CUSTOMER)) {
                Long customerId = HttpUtil.checkSession(req);
                printWriter.println(gson.toJson(FactoryDao.getCustomerDao().getCustomerById(customerId, false)));
                return;

            } else if (uri.contains(LOGIN_FORM)) {
                resp.setContentType("text/html");
                printWriter.println("<html><body><script>window.location.href=\"login.html\"</script></body></html>");
                return;
            } else if (uri.contains(SEARCH_CUSTOMERS)) {
                HttpUtil.checkSession(req);
                String[] pathParts = pathInfo.split("/");
                if (pathParts.length != 2) {
                	printWriter.println(gson.toJson(FactoryDao.getCustomerDao().searchCustomers("")));
                }
                String searchText = pathParts[1];
                printWriter.println(gson.toJson(FactoryDao.getCustomerDao().searchCustomers(searchText)));
                return;
            }

            throw new UnsupportedFunction();
        } catch (ConnectMeException e) {
            Log.e(TAG, "doGet", e.getMessage(), e);
            HttpUtil.sendCustomError(e, printWriter, resp);
        } catch (Exception e) {
            Log.e(TAG, "doGet", e.getMessage(), e);
            HttpUtil.sendError(printWriter, resp);
        }finally {
            Log.l(TAG, "doGet", req.getRequestURI() + " Finished" + ot.close());
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        OT ot = new OT();
        PrintWriter printWriter = resp.getWriter();
        Gson gson = new Gson();
        try {

            String uri = req.getRequestURI();
            Log.l(TAG, "doPost", String.format("POST Request URI: %s", uri));
            resp.setContentType("application/json");
            String postDataStr = HttpUtil.getPostData(req);

            if (uri.endsWith(API_LOGIN)) {
                LoginRequest loginRequest = gson.fromJson(postDataStr, LoginRequest.class);
                if (loginRequest == null || !loginRequest.isValid()) {
                    throw new UsernameOrPasswordIsIncorrect();
                }
                Customer customer = FactoryDao.getCustomerDao().checkCredential(loginRequest);
                HttpUtil.setSession(req, customer.getId());
                printWriter.println(gson.toJson(FactoryDao.getCustomerDao().getCustomerById(customer.getId(), false)));
                return;
            } else if (uri.endsWith(API_LOGOUT)) {
                HttpUtil.invalidateSession(req);
                resp.sendRedirect("../login.html");
                return;
            } else if (uri.endsWith(API_SIGN_UP)) {
                SignUpRequest signUpRequest = gson.fromJson(postDataStr, SignUpRequest.class);
                Customer customer = FactoryDao.getCustomerDao().addNewCustomer(signUpRequest);
                HttpUtil.setSession(req, customer.getId());
                printWriter.println(gson.toJson(customer));
                return;

            } else if (uri.endsWith(API_CHANGE_PASSWORD)) {
                Long customerId = HttpUtil.checkSession(req);
                ChangePasswordRequest changePasswordRequest = gson.fromJson(postDataStr, ChangePasswordRequest.class);
                FactoryDao.getCustomerDao().changePassword(changePasswordRequest, customerId);
                printWriter.println("{\"success\":true}");
                printWriter.println(gson.toJson(new SuccessResponse("Password changed successfully")));
                return;
            } else if (uri.endsWith(API_FORGET_PASSWORD)) {
                ForgetPasswordRequest forgetPassword = gson.fromJson(postDataStr, ForgetPasswordRequest.class);
                FactoryDao.getCustomerDao().forgetPassword(forgetPassword);
                printWriter.println(gson.toJson(new SuccessResponse("Password changed successfully")));
                return;
            }

            throw new UnsupportedFunction();
        } catch (ConnectMeException e) {
            Log.e(TAG, "doPost", e.getMessage(), e);
            HttpUtil.sendCustomError(e, printWriter, resp);
        } catch (Exception e) {
            Log.e(TAG, "doPost", e.getMessage(), e);
            HttpUtil.sendError(printWriter, resp);
        } finally {
            Log.l(TAG, "doPost", req.getRequestURI() + " Finished" + ot.close());
        }

    }
}
