package com.connectme.servlet;

import com.connectme.dao.FactoryDao;
import com.connectme.enums.ChannelType;
import com.connectme.exception.ChannelNotFound;
import com.connectme.exception.ConnectMeException;
import com.connectme.exception.CustomerNotFound;
import com.connectme.exception.UnsupportedFunction;
import com.connectme.model.Channel;
import com.connectme.model.ChannelMessage;
import com.connectme.model.Customer;
import com.connectme.model.PublicChannel;
import com.connectme.requests.*;
import com.connectme.util.HttpUtil;
import com.connectme.util.Log;
import com.connectme.util.OT;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet(
        name = "ChannelServlet",
        description = "Channel servlet manage all channels operations",
        urlPatterns = {
                "/api/channels",
                "/api/channel",
                "/api/channel/messages/*",
                "/api/channel/message",
                "/api/subscribe",
                "/api/unsubscribe",
                "/api/search/channel/*",
                "/api/channel/readall"
        }
)
public class ChannelServlet extends HttpServlet {

    private static final long serialVersionUID = -24774005733232585L;
    private static final String TAG = ChannelServlet.class.getSimpleName();
    private static final String GET_MY_CHANNELS = "/api/channels";
    private static final String GET_CHANNEL_MESSAGES = "/api/channel/messages/";

    private static final String ADD_NEW_CHANNEL_MESSAGE = "/api/channel/message";
    private static final String ADD_NEW_CHANNEL = "/api/channel";
    private static final String SUBSCRIBE_TO_CHANNEL = "/api/subscribe";
    private static final String UNSUBSCRIBE_TO_CHANNEL = "/api/unsubscribe";
    private static final String SEARCH_CHANNELS = "/api/search/channel/";
    private static final String READ_ALL_MESSAGES = "/api/channel/readall";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OT ot = new OT();
        PrintWriter printWriter = resp.getWriter();
        Gson gson = new Gson();
        try {

            String uri = req.getRequestURI();
            resp.setContentType("application/json");
            Log.l(TAG, "goGet", String.format("GET Request URI: %s", uri));
            String pathInfo = req.getPathInfo();

            if (uri.endsWith(GET_MY_CHANNELS)) {

                Long customerId = HttpUtil.checkSession(req);

                Customer customer = FactoryDao.getCustomerDao().getCustomerById(customerId, true);

                List<Channel> privateChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PRIVATE);
                List<Channel> publicChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PUBLIC);

                printWriter.println(gson.toJson(new ChannelResponse(privateChannels, publicChannels)));
                return;

            } else if (uri.contains(GET_CHANNEL_MESSAGES)) {
                Long customerId = HttpUtil.checkSession(req);

                String[] pathParts = pathInfo.split("/");
                if (pathParts.length != 3) {
                    throw new ChannelNotFound();
                }
                String channelIdStr = pathParts[1];
                Long channelId;
                try {
                    channelId = Long.parseLong(channelIdStr);
                } catch (Exception e) {
                    throw new ChannelNotFound();
                }
                String offsetDateStr = pathParts[2];
                Long offsetDate;
                try {
                    offsetDate = Long.parseLong(offsetDateStr);
                } catch (Exception e) {
                    throw new ChannelNotFound();
                }

                printWriter.println(gson.toJson(FactoryDao.getChannelMessageDao().getMassageByChannelAndOffset(
                        FactoryDao.getCustomerDao().getCustomerById(customerId, true),
                        channelId, offsetDate)));
                return;
            } else if (uri.contains(SEARCH_CHANNELS)) {
                HttpUtil.checkSession(req);
                String[] pathParts = pathInfo.split("/");
                if (pathParts.length != 2) {
                    throw new CustomerNotFound();
                }
                String searchText = pathParts[1];
                printWriter.println(gson.toJson(FactoryDao.getChannelDao().searchPublicChannels(searchText)));
                return;
            }

            throw new UnsupportedFunction();
        } catch (ConnectMeException e) {
            Log.e(TAG, "doGet", e.getMessage(), e);
            HttpUtil.sendCustomError(e, printWriter, resp);
        } catch (Exception e) {
            Log.e(TAG, "doGet", e.getMessage(), e);
            HttpUtil.sendError(printWriter, resp);
        } finally {
            Log.l(TAG, "doGet", req.getRequestURI() + " Finished" + ot.close());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        OT ot = new OT();
        PrintWriter printWriter = resp.getWriter();
        Gson gson = new Gson();
        try {

            String uri = req.getRequestURI();
            Log.l(TAG, "doPost", String.format("POST Request URI: %s", uri));
            resp.setContentType("application/json");

            String postDataStr = HttpUtil.getPostData(req);
            if (uri.endsWith(ADD_NEW_CHANNEL_MESSAGE)) {
                Customer customer = FactoryDao.getCustomerDao().getCustomerById(HttpUtil.checkSession(req), true);
                AddNewChannelMessageRequest request = gson.fromJson(postDataStr, AddNewChannelMessageRequest.class);
                Channel channel = FactoryDao.getChannelDao().getChannelById((long) request.channelId);
                printWriter.println(gson.toJson(FactoryDao.getChannelMessageDao().addNewMessage(channel, customer, request.message)));

                return;
            }
            if (uri.endsWith(ADD_NEW_CHANNEL)) {
                AddNewChannelRequest request = gson.fromJson(postDataStr, AddNewChannelRequest.class);
                Customer customer = FactoryDao.getCustomerDao().getCustomerById(HttpUtil.checkSession(req), true);
                if (request.type == ChannelType.PRIVATE.ordinal()) {
                    Customer destination = FactoryDao.getCustomerDao().getCustomerById((long) request.customerId, true);
                    FactoryDao.getChannelDao().createNewPrivateChannel(customer, destination, request.name, request.desc);
                } else {
                    FactoryDao.getChannelDao().createNewPublicChannel(customer, request.name, request.desc);
                }

                // return the updates channels list
                List<Channel> privateChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PRIVATE);
                List<Channel> publicChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PUBLIC);
                printWriter.println(gson.toJson(new ChannelResponse(privateChannels, publicChannels)));
                return;
            }
            if (uri.endsWith(SUBSCRIBE_TO_CHANNEL)) {
                ChannelSubscribeRequest request = gson.fromJson(postDataStr, ChannelSubscribeRequest.class);
                Customer customer = FactoryDao.getCustomerDao().getCustomerById(HttpUtil.checkSession(req), true);
                Channel channel = FactoryDao.getChannelDao().getChannelById((long) request.channelId);
                FactoryDao.getChannelCustomerRelationDao().addCustomerToPublicChannel(customer, (PublicChannel) channel);


                // return the updates channels list
                List<Channel> privateChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PRIVATE);
                List<Channel> publicChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PUBLIC);
                printWriter.println(gson.toJson(new ChannelResponse(privateChannels, publicChannels)));
                return;
            }
            if (uri.endsWith(UNSUBSCRIBE_TO_CHANNEL)) {
                ChannelSubscribeRequest request = gson.fromJson(postDataStr, ChannelSubscribeRequest.class);
                Customer customer = FactoryDao.getCustomerDao().getCustomerById(HttpUtil.checkSession(req), true);
                Channel channel = FactoryDao.getChannelDao().getChannelById((long) request.channelId);
                FactoryDao.getChannelCustomerRelationDao().removeCustomerFromPublicChannel(customer, (PublicChannel) channel);


                // return the updates channels list
                List<Channel> privateChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PRIVATE);
                List<Channel> publicChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PUBLIC);
                printWriter.println(gson.toJson(new ChannelResponse(privateChannels, publicChannels)));
                return;
            }

            if (uri.endsWith(READ_ALL_MESSAGES)) {
                Customer customer = FactoryDao.getCustomerDao().getCustomerById(HttpUtil.checkSession(req), true);
                ChannelSubscribeRequest request = gson.fromJson(postDataStr, ChannelSubscribeRequest.class);
                FactoryDao.getChannelMessageDao().readAllMessagesInChannel(customer, (long) request.channelId);

                List<Channel> privateChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PRIVATE);
                List<Channel> publicChannels = FactoryDao.getChannelDao().getAllCustomerChannelsByType(customer, ChannelType.PUBLIC);
                printWriter.println(gson.toJson(new ChannelResponse(privateChannels, publicChannels)));
                return;
            }

            throw new UnsupportedFunction();

        } catch (ConnectMeException e) {
            Log.e(TAG, "doPost", e.getMessage(), e);
            HttpUtil.sendCustomError(e, printWriter, resp);
        } catch (Exception e) {
            Log.e(TAG, "doPost", e.getMessage(), e);
            HttpUtil.sendError(printWriter, resp);
        } finally {
            Log.l(TAG, "doPost", req.getRequestURI() + " Finished" + ot.close());
        }
    }
}
