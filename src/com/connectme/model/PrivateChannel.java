package com.connectme.model;

import static com.connectme.enums.ChannelType.PRIVATE;

public class PrivateChannel extends Channel {

    /**
     * Default Constructor
     */
    public PrivateChannel() {
        super(PRIVATE);
    }

    public PrivateChannel(Long id, String name, String shortDesc, Long createdByCustomerId, Long createdAt, Long updatedAt) {
        super(PRIVATE, id, name, shortDesc, createdByCustomerId, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
