package com.connectme.model;

import com.connectme.enums.MessageType;
import com.google.gson.Gson;

public class ChannelMessage {

    private Long id;
    private Long customerId;
    private String message;
    private Long createdAt;
    private Long updatedAt;
    // Read by users contains the user ids separated by comma
    private String readBy;
    private String mentions;
    private MessageType type;
    private Long parentId;
    private Long channelId;

    // for transit only
    private Customer customer;

    /**
     * Default Constructor
     */
    public ChannelMessage() {
    }

    public ChannelMessage(Long id, Long customerId, String message, Long createdAt, Long updatedAt, String readBy, String mentions, MessageType type, Long parentId, Long channelId) {
        this.id = id;
        this.customerId = customerId;
        this.message = message;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.readBy = readBy;
        this.mentions = mentions;
        this.type = type;
        this.parentId = parentId;
        this.channelId = channelId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getReadBy() {
        return readBy;
    }

    public void setReadBy(String readBy) {
        this.readBy = readBy;
    }

    public String getMentions() {
        return mentions;
    }

    public void setMentions(String mentions) {
        this.mentions = mentions;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    /**
     * Split the readBy field to retrieve it as array
     *
     * @return {@link String[]} of customerId ids
     */
    public String[] getReadByList() {
        return readBy.split(",");
    }

    /**
     * Join the array of customerId ids and set it in the readBy field as string separated by comma
     *
     * @param ids {@link String[]} of customerId ids
     */
    public void setReadByList(String[] ids) {
        String readBy = "";
        for (int i = 0; i < ids.length; i++) {
            readBy += ids[i] + (i < ids.length - 1 ? "," : "");
        }
        this.readBy = readBy;
    }

    /**
     * Split the mentions field to retrieve it as array
     *
     * @return {@link String[]} of customerId ids
     */
    public String[] getMentionsList() {
        return mentions.split(",");
    }

    /**
     * Join the array of customerId ids and set it in the mentions field as string separated by comma
     *
     * @param ids {@link String[]} of customerId ids
     */
    public void setMentionsList(String[] ids) {
        String readBy = "";
        for (int i = 0; i < ids.length; i++) {
            readBy += ids[i] + (i < ids.length - 1 ? "," : "");
        }
        this.mentions = readBy;
    }

    public boolean isReadByCustomer(Long id) {
        return readBy.contains(id + "");
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "ChannelMessage{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", message='" + message + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", readBy='" + readBy + '\'' +
                ", mentions='" + mentions + '\'' +
                ", type=" + type +
                ", parentId=" + parentId +
                ", channelId=" + channelId +
                '}';
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
