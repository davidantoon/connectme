package com.connectme.model;


/**
 * Class ChannelCustomerRelation contains two fields with unique (${@link ChannelCustomerRelation#id})
 * and unique group (${@link ChannelCustomerRelation#customerId}, ${@link ChannelCustomerRelation#channelId})
 */
public class ChannelCustomerRelation {

    private Long id;
    private Long customerId;
    private Long channelId;
    private Long createdAt;

    /**
     * Default Constructor
     */
    public ChannelCustomerRelation() {
    }

    public ChannelCustomerRelation(Long id, Long customerId, Long channelId, Long createdAt) {
        this.id = id;
        this.customerId = customerId;
        this.channelId = channelId;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "ChannelCustomerRelation{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", channelId=" + channelId +
                ", createdAt=" + createdAt +
                '}';
    }
}
