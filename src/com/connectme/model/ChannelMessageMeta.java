package com.connectme.model;

public class ChannelMessageMeta {
    private Integer count;
    private ChannelMessage lastMessage;
    private Boolean isMentioned;

    public ChannelMessageMeta() {
    }

    public ChannelMessageMeta(Integer count, ChannelMessage lastMessage, Boolean isMentioned) {
        this.count = count;
        this.lastMessage = lastMessage;
        this.isMentioned = isMentioned;
    }

    public Integer getCount() {
        return count;
    }

    public ChannelMessage getLastMessage() {
        return lastMessage;
    }

    public Boolean getMentioned() {
        return isMentioned;
    }
}
