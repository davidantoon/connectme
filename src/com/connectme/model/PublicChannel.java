package com.connectme.model;

import static com.connectme.enums.ChannelType.PUBLIC;


public class PublicChannel extends Channel {

    /**
     * Default Constructor
     */
    public PublicChannel() {
        super(PUBLIC);
    }

    public PublicChannel(Long id, String name, String shortDesc, Long createdByCustomerId, Long createdAt, Long updatedAt) {
        super(PUBLIC, id, name, shortDesc, createdByCustomerId, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
