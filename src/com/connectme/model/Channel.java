package com.connectme.model;

import com.connectme.enums.ChannelType;
import com.google.gson.Gson;

import java.util.List;

public class Channel {

    private Long id;
    private String name;
    private String shortDesc;
    private Long createdByCustomerId;
    private Long createdAt;
    private Long updatedAt;
    private ChannelType type;
    private ChannelMessageMeta metadata;

    // For transit only
    private List<Long> subscribers;

    /**
     * Default Type Constructor
     *
     * @param type the type of the channel
     */
    public Channel(ChannelType type) {
        this.type = type;
    }

    public Channel(ChannelType type, Long id, String name, String shortDesc, Long createdByCustomerId, Long createdAt, Long updatedAt) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.shortDesc = shortDesc;
        this.createdByCustomerId = createdByCustomerId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public Long getCreatedByCustomerId() {
        return createdByCustomerId;
    }

    public void setCreatedByCustomerId(Long createdByCustomerId) {
        this.createdByCustomerId = createdByCustomerId;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ChannelType getType() {
        return type;
    }

    public void setType(ChannelType type) {
        this.type = type;
    }


    public List<Long> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Long> subscribers) {
        this.subscribers = subscribers;
    }

    public ChannelMessageMeta getMetadata() {
        return metadata;
    }

    public void setMetadata(ChannelMessageMeta metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", shortDesc='" + shortDesc + '\'' +
                ", createdByCustomerId=" + createdByCustomerId +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", type=" + type +
                '}';
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
