package com.connectme.util;

public class Consts {


    /**********************************
     * Database Configurations
     **********************************/
    private static final String DB_NAME = "projectDB";
    private static final String DB_PROTOCOL = "jdbc:derby:";
    public static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String DB_USERNAME = "username";
    public static final String DB_PASSWORD = "password";
    public static final Integer DB_MAX_CONNECTIONS = 100;
    public static final String DB_CONNECTION_URL = DB_PROTOCOL + DB_NAME + ";create=true";
    public static final String SESSION_CUSTOMER_KEY = "customerId";
    public static final String DUMMY_CUSTOMERS_DATA_FILE = "/dummy_customers.json";
    public static final String DUMMY_PUBLIC_CHANNELS_DATA_FILE = "dummy_public_channels.json";
}
