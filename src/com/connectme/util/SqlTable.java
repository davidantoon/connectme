package com.connectme.util;

public class SqlTable {
    public static final String CUSTOMERS_TABLE = "CUSTOMERS_DT";
    public static final String CHANNELS_TABLE = "CHANNELS_DT";
    public static final String CHANNEL_CUSTOMER_RELATION_TABLE = "CHANNEL_CUSTOMER_RELATION_DT";
    public static final String CHANNEL_MESSAGE_TABLE = "CHANNEL_MESSAGES_DT";
}
