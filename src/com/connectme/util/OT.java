package com.connectme.util;


/**
 * OT Class for Optimization Time,
 * On Constructor saves the current time and when close return the diff between
 * current time and started time.
 * The main purpose is to explore specific function's execute duration
 */
public class OT {

    private Long startTime;

    public OT() {
        this.startTime = System.currentTimeMillis();
    }

    public String close() {
        return " (" + (System.currentTimeMillis() - startTime) + "ms)";
    }
}
