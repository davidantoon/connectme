package com.connectme.util;

public class ValidateInput {

    private static boolean isNotNull(String str) {
        return str != null;
    }

    public static boolean isNotNullOrEmpty(String str) {
        return isNotNull(str) && !str.isEmpty() && !str.trim().isEmpty();
    }

    public static boolean isInRange(String str, int from, int to) {
        return isNotNull(str) && str.length() >= from && str.length() <= to;
    }

    public static boolean isValidUsername(String username) {
        return isInRange(username, 1, 10) && !username.contains(" ");
    }

    public static boolean isValidPassword(String password) {
        return isInRange(password, 1, 8);
    }
}
