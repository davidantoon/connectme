package com.connectme.util;

import static com.connectme.util.SqlTable.*;

public class SqlStmt {

    /********************************
     * Create SQL Tables Statements
     *******************************/
    public static final String CREATE_CUSTOMERS_TABLE_STMT = "CREATE TABLE " + CUSTOMERS_TABLE + " (" +
            "id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
            "username VARCHAR(10) NOT NULL UNIQUE, " +
            "password VARCHAR(8) NOT NULL, " +
            "nickname VARCHAR(20) NOT NULL UNIQUE, " +
            "shortdesc VARCHAR(50) NOT NULL, " +
            "photo VARCHAR(255), " +
            "createdat REAL NOT NULL, " +
            "updatedat REAL NOT NULL, " +
            "securityquestion VARCHAR(255) NOT NULL, " +
            "securityanswer VARCHAR(255) NOT NULL)";
    public static final String CREATE_CUSTOMER_CHANNEL_RELATION_TABLE_STMT = "CREATE TABLE " + CHANNEL_CUSTOMER_RELATION_TABLE + " (" +
            "id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
            "customerid REAL NOT NULL, " +
            "channelid REAL NOT NULL, " +
            "createdat REAL NOT NULL)";
    public static final String CREATE_CHANNELS_TABLE_STMT = "CREATE TABLE " + CHANNELS_TABLE + " (" +
            "id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
            "name VARCHAR(30) NOT NULL, " +
            "shortdesc VARCHAR(500) NOT NULL, " +
            "createdbycustomerid REAL NOT NULL, " +
            "createdat REAL NOT NULL, " +
            "updatedat REAL NOT NULL, " +
            "type INTEGER NOT NULL)";
    public static final String CREATE_CHANNEL_MESSAGE_TABLE_STMT = "CREATE TABLE " + CHANNEL_MESSAGE_TABLE + " (" +
            "id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
            "customerid REAL NOT NULL, " +
            "message VARCHAR(500) NOT NULL, " +
            "createdat REAL NOT NULL, " +
            "updatedat REAL NOT NULL, " +
            "readby VARCHAR(2000) NOT NULL, " +
            "mentions VARCHAR(2000) NOT NULL, " +
            "type INTEGER NOT NULL, " +
            "parentid REAL, " +
            "channelid REAL NOT NULL)";


    /**************************************************************
     * Customers SQL Statements
     *************************************************************/
    public static final String GET_ALL_CUSTOMERS = "SELECT * FROM " + CUSTOMERS_TABLE;
    public static final String GET_CUSTOMER_WHERE_CUSTOMER_ID = "SELECT * FROM " + CUSTOMERS_TABLE + " WHERE id=?";
    public static final String GET_CUSTOMER_WHERE_USERNAME = "SELECT * FROM " + CUSTOMERS_TABLE + " WHERE username=?";
    public static final String GET_CUSTOMER_WHERE_CUSTOMER_NICKNAME = "SELECT * FROM " + CUSTOMERS_TABLE + " WHERE nickname=?";
    public static final String INSERT_NEW_CUSTOMER = "INSERT INTO " + CUSTOMERS_TABLE +
            " (username,password,nickname,shortdesc,photo,createdat,updatedat,securityquestion,securityanswer)" +
            " VALUES(?,?,?,?,?,?,?,?,?)";
    public static final String UPDATE_CUSTOMER_DATA_WHERE_CUSTOMER_ID = "UPDATE " + CUSTOMERS_TABLE +
            " SET nickname=?,shortdesc=?,photo=?,updatedat=?" +
            " WHERE id=?";
    public static final String UPDATE_CUSTOMER_PASSWORD_WHERE_OLD_PASSWORD_AND_CUSTOMER_ID = "UPDATE " + CUSTOMERS_TABLE +
            " SET password=?,updatedat=?" +
            " WHERE id=? AND password=?";

    public static final String UPDATE_CUSTOMER_PASSWORD_WHERE_USERNAME_SECURITY_Q_A = "UPDATE " + CUSTOMERS_TABLE +
            " SET password=?,updatedat=?" +
            " WHERE username=? AND securityquestion=? AND securityanswer=?";

    public static final String GET_CUSTOMER_WHERE_USERNAME_AND_PASSWORD = "SELECT * FROM " + CUSTOMERS_TABLE + " " +
            "WHERE username=? AND password=?";
    public static final String SEARCH_CUSTOMER_WHERE_NICKNAME = "SELECT * FROM " + CUSTOMERS_TABLE + " WHERE "
    		+ "nickname LIKE '%LIKE_HOLDER%' FETCH FIRST 10 ROWS ONLY";


    /**************************************************************
     * ChannelCustomerRelations SQL Statements
     *************************************************************/
    public static final String GET_CHANNEL_CUSTOMER_RELATION_BY_ID = "SELECT * FROM " + CHANNEL_CUSTOMER_RELATION_TABLE +
            " WHERE id=?";
    public static final String GET_CHANNEL_CUSTOMER_RELATION_BY_CHANNEL_ID = "SELECT * FROM " + CHANNEL_CUSTOMER_RELATION_TABLE +
            " WHERE channelid=?";
    public static final String GET_CHANNEL_CUSTOMER_RELATION_BY_CUSTOMER_ID = "SELECT * FROM " + CHANNEL_CUSTOMER_RELATION_TABLE +
            " WHERE customerid=?";
    public static final String GET_CHANNEL_CUSTOMER_RELATION_BY_CHANNEL_ID_AND_CUSTOMER_ID = "SELECT * FROM " + CHANNEL_CUSTOMER_RELATION_TABLE +
            " WHERE channelid=? AND customerid=?";
    public static final String INSERT_NEW_CHANNEL_CUSTOMER_RELATION = "INSERT INTO " + CHANNEL_CUSTOMER_RELATION_TABLE +
            " (customerid, channelid, createdat) VALUES(?,?,?)";
    public static final String DELETE_CHANNEL_CUSTOMER_RELATION = "DELETE FROM " + CHANNEL_CUSTOMER_RELATION_TABLE +
            " WHERE customerid=? AND channelid=?";


    /**************************************************************
     * Channels SQL Statements
     *************************************************************/
    public static final String GET_CHANNEL_BY_ID = "SELECT * FROM " + CHANNELS_TABLE + " WHERE id=?";
    public static final String INSERT_NEW_CHANNEL = "INSERT INTO " + CHANNELS_TABLE +
            " (name,shortDesc,createdByCustomerId,createdAt,updatedAt,type) VALUES(?,?,?,?,?,?)";

    public static final String GET_ALL_CUSTOMER_CHANNELS_BY_TYPE = "SELECT * FROM " + CHANNELS_TABLE +
            " WHERE type=? AND id IN (SELECT channelid FROM " + CHANNEL_CUSTOMER_RELATION_TABLE + " WHERE customerid=?)";

    public static final String SEARCH_PUBLIC_CHANNEL_WHERE_NAME = "SELECT * FROM " + CHANNELS_TABLE + " WHERE "
    		+ "type=0 AND name LIKE '%LIKE_HOLDER%'";

    /**************************************************************
     * Channel Messages SQL Statements
     *************************************************************/
    public static final String INSERT_NEW_CHANNEL_MESSAGE = "INSERT INTO " + CHANNEL_MESSAGE_TABLE +
            " (customerid,message,createdat,updatedat,readby,mentions,type,parentid,channelid)" +
            " VALUES(?,?,?,?,?,?,?,?,?)";
    public static final String MARK_MESSAGE_AS_READ = "UPDATE " + CHANNEL_MESSAGE_TABLE + " SET readby=? WHERE id=?";
    public static final String GET_CHANNEL_MESSAGES = "SELECT * FROM " + CHANNEL_MESSAGE_TABLE + " WHERE " +
            "channelid=? AND createdat<=? ORDER BY createdat DESC " +
            "FETCH FIRST 10 ROWS ONLY";

    public static final String GET_CHANNEL_UNREAD_MESSAGES = "SELECT * FROM " + CHANNEL_MESSAGE_TABLE + " WHERE " +
            "channelid=? AND createdat>=? AND ( NOT (',' || readby || ',') LIKE '%,LIKE_HOLDER,%')";

    public static final String GET_CHANNEL_MESSAGE_BY_ID = "SELECT * FROM " + CHANNEL_MESSAGE_TABLE +
            " WHERE id=?";
}
