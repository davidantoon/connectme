package com.connectme.util;

import com.connectme.exception.ConnectMeException;
import com.connectme.exception.NotAuthorizedRequest;
import com.connectme.exception.UnknownException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.PrintWriter;

public class HttpUtil {

    public static Long checkSession(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession(false);
        if (session != null) {
            try {
                return Long.parseLong(session.getAttribute(Consts.SESSION_CUSTOMER_KEY).toString());
            } catch (Exception e) {
                throw new NotAuthorizedRequest();
            }
        }
        throw new NotAuthorizedRequest();
    }

    public static void setSession(HttpServletRequest request, Long customerId) {
        HttpSession session = request.getSession(true);
        session.setAttribute(Consts.SESSION_CUSTOMER_KEY, customerId);
    }

    public static void invalidateSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }

    public static void sendError(PrintWriter printWriter, HttpServletResponse resp) {
        resp.setStatus(500);
        printWriter.println("{\"code\":500,\"message\":\"" + UnknownException.MESSAGE + "\"}");
    }

    public static void sendCustomError(ConnectMeException e, PrintWriter printWriter, HttpServletResponse resp) {
        resp.setStatus(400);
        resp.setContentType("application/json");
        printWriter.println("{\"code\":" + e.getCode() + ",\"message\":\"" + e.getMessage() + "\"}");
    }

    public static String getPostData(HttpServletRequest req) throws Exception {

        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = req.getReader()) {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
            return sb.toString();
        }
    }
}
