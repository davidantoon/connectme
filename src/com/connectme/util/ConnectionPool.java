package com.connectme.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ConnectionPool {

    private static final String TAG = ConnectionPool.class.getSimpleName();
    private String driver, url, username, password;
    private int maxConnections;
    private List<Connection> connections;
    private boolean connectionPending = false;

    public ConnectionPool(String driver, String url, String username, String password, int maxConnections) throws SQLException {
        Log.lt(TAG, "Construction", "Init DB Connection Pool");
        this.driver = driver;
        this.url = url;
        this.username = username;
        this.password = password;
        this.maxConnections = maxConnections;
        this.connections = new ArrayList<>();
    }

    public synchronized Connection getConnection() throws SQLException {

        Log.l(TAG, "getConnection", this.connections.size() + " running connections");
        if (this.connections.size() > maxConnections) {
            // Sleep random milli seconds and try again
            Long milli = (long) (new Random().nextInt(10) * 100);
            Log.l(TAG, "getConnection", "Too many connections wait " + milli);
            try {
                Thread.sleep(milli);
            } catch (InterruptedException ignore) {
            }

            // Creating new connection
            Connection con = makeNewConnection();
            connections.add(con);
            return con;
        }

        // Creating new connection
        Connection con = makeNewConnection();
        connections.add(con);
        return con;
    }

    /**
     * Close all the connections. Use with caution: be sure no connections are
     * in use before calling. Note that you are not <I>required to call this
     * when done with a ConnectionPool, since connections are guaranteed to be
     * closed when garbage collected. But this method gives more control
     * regarding when the connections are closed.
     */
    public synchronized void shutdown() {
        closeConnections(connections);
    }

    public synchronized String toString() {
        return ("ConnectionPool(" + url + "," + username + ")" + ", busy=" + connections.size() + ", max=" + maxConnections);
    }

    /**
     * // Private methods. Under the assumption that they are called
     * // from the public synchronized methods. Do not call them from
     * // a non synced method!
     * // This explicitly makes a new connection. Called in
     * // the foreground when initializing the ConnectionPool,
     * // and called in the background when running.
     */
    private Connection makeNewConnection() throws SQLException {
        try {
            // Load database driver if not already loaded
            Class.forName(driver).newInstance();

            // Establish network connection to database
            return DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException ignore) {
            // Simplify try/catch blocks of people using this by
            // throwing only one exception type.
            throw new SQLException("Can't find class for driver: " + driver);
        } catch (IllegalAccessException ignore) {
            // Simplify try/catch blocks of people using this by
            // throwing only one exception type.
            throw new SQLException("Can't access DB with username: " + username + " password: " + password);
        } catch (InstantiationException ignore) {
            // Simplify try/catch blocks of people using this by
            // throwing only one exception type.
            throw new SQLException("Can't access DB with url: " + url);
        }
    }

    private void closeConnections(List<Connection> connections) {
        try {
            for (Connection connection : connections) {
                if (!connection.isClosed()) {
                    connection.close();
                }
            }
        } catch (SQLException ignore) {
        }
    }

    public synchronized void removeClosedConnections() throws Exception {
    	List<Connection> newConn = new ArrayList<>(); 
        for (Connection connection : connections) {
            if (!connection.isClosed()) {
            	newConn.add(connection);
            }
        }
        connections = newConn;
    }
}
