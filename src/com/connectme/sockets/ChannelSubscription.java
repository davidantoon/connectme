package com.connectme.sockets;


import com.connectme.enums.ChannelNotificationType;
import com.connectme.model.Channel;
import com.connectme.model.ChannelMessage;
import com.connectme.model.Customer;
import com.connectme.util.Log;

import javax.websocket.Session;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ChannelSubscription {

    private static final String TAG = ChannelSubscription.class.getSimpleName();
    /**
     * Holds all channel subscriptions and store by channel key and notify by channel id
     * Map:
     * - Key        (Long)              the channel Id
     * - Value      (ChannelHolder)     the channel sessions holder
     */
    private Map<Long, ChannelHolder> holders = new HashMap<>();
    /**
     * Holds all customers socket sessions to send customer private events
     * Map:
     * - Key        (Long)              the channel Id
     * - Value      (ChannelHolder)     the channel sessions holder
     */
    private Map<Long, Session> customerHolders = new HashMap<>();

    public void unSubscribeFromAll(Session session) {
        for (Long channelId : holders.keySet()) {
            holders.get(channelId).unsubscribe(session);
        }
    }

    public void unSubscribeFromAll(Customer customer) {
        for (Long channelId : holders.keySet()) {
            holders.get(channelId).unsubscribe(customer);
            Log.l(TAG, "subscribeToChannel", "Customer with id: " + customer.getId() +
                    " UN-SUBSCRIBED from channel with id: " + channelId);
        }
    }

    public void subscribeToChannel(Long channelId, Customer customer, Session session) {
        // if non subscribed users to channel, create new holders
        if (!holders.containsKey(channelId)) {
            holders.put(channelId, new ChannelHolder(channelId));
        }

        holders.get(channelId).subscribe(session, customer);

        Log.l(TAG, "subscribeToChannel", "Customer with id: " + customer.getId() +
                " SUBSCRIBED to channel with id: " + channelId);
    }

    public void subscribeToGlobalEvents(Customer customer, Session session) {
        customerHolders.put(customer.getId(), session);
        Log.l(TAG, "subscribeToChannel", "Customer with id: " + customer.getId() +
                " SUBSCRIBED to global events");
    }

    public void unSubscribeFromGlobalEvents(Session session) {
        for (Long customerId : customerHolders.keySet()) {
            if (customerHolders.get(customerId).getId().equals(session.getId())) {
                customerHolders.remove(customerId);
                Log.l(TAG, "subscribeToChannel", "Customer with id: " + customerId + " UN-SUBSCRIBED from global events");
                return;
            }
        }
    }

    public void notifyChannel(Channel channel, Customer customer, ChannelMessage channelMessage, ChannelNotificationType type) {
        if(holders.get(channel.getId()) == null)
			return;
		Set<Session> sessionList = holders.get(channel.getId()).getSessions();
		for (Session session : sessionList) {
			try{
				session.getBasicRemote().sendText("{\"type\":\"" + type.name() + "\", \"data\":" + channelMessage.toJson() + "}");
			}catch(Exception ignore){
				
			}
		}
    }

    public void notifyCustomer(Channel channel, Customer customer, ChannelNotificationType type) {
        try {
        	if(customerHolders.get(customer.getId()) == null)
        		return;
            customerHolders.get(customer.getId()).getBasicRemote().sendText("{\"type\":\"" + type.name() + "\", \"data\":" + channel.toJson() + "}");
        } catch (Exception e) {
            Log.e(TAG, "notifyCustomer", "Failed to send socket message", e);
        }
    }

    public void shutdown() {
        for (Long channelId : holders.keySet()) {
            try {
                holders.get(channelId).shutdown();
            } catch (Exception ignore) {
            }
        }
        for (Long customerId : customerHolders.keySet()) {
            try {
                customerHolders.get(customerId).close();
            } catch (Exception ignore) {
            }
            try {
                customerHolders.remove(customerId);
            } catch (Exception ignore) {
            }
        }
    }

    public Session getSessionByCustomer(Long customerId) {
        return customerHolders.get(customerId);
    }
}
