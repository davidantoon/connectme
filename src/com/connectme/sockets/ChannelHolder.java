package com.connectme.sockets;

import com.connectme.model.Customer;
import com.connectme.util.Log;

import javax.websocket.Session;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ChannelHolder {

    private static final String TAG = ChannelHolder.class.getSimpleName();
    private Long channelId;
    private HashMap<Session, Customer> sessions;

    public ChannelHolder(Long channelId) {
        this.channelId = channelId;
        this.sessions = new HashMap<>();
    }

    public void unsubscribe(Session session) {
        Customer customer = sessions.get(session);
        if(session == null)
        	return;
        sessions.remove(session);
        Log.l(TAG, "unsubscribe", "Session: " + session.getId() + " (" + customer.getNickname() + ") unsubscribed from channel: " + this.channelId);
    }

    public void unsubscribe(Customer customer) {
        for (Session session : sessions.keySet()) {
            if (sessions.get(session).getId().equals(customer.getId())) {
                unsubscribe(session);
                return;
            }
        }
    }

    public void subscribe(Session session, Customer customer) {
        sessions.put(session, customer);
        Log.l(TAG, "subscribe", "Session: " + session.getId() + " subscribed for channel: " + this.channelId);
    }

    public Long getChannelId() {
        return channelId;
    }

    public void shutdown() {
        for (Session sesstion : sessions.keySet()) {
            try {
                sesstion.close();
            } catch (Exception ignore) {
            }
        }
    }

    public Set<Session> getSessions() {
        return sessions.keySet();
    }
}
