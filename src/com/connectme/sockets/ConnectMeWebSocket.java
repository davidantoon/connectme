package com.connectme.sockets;

import com.connectme.ConnectMeApp;
import com.connectme.dao.FactoryDao;
import com.connectme.model.ChannelCustomerRelation;
import com.connectme.model.Customer;
import com.connectme.util.Consts;
import com.connectme.util.Log;
import com.connectme.util.OT;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;

@ServerEndpoint(value = "/socket", configurator = GetHttpSessionConfigurator.class)
public class ConnectMeWebSocket {

    private static final String TAG = ConnectMeWebSocket.class.getSimpleName();
    private Session wsSession;
    private HttpSession httpSession;

    @OnOpen
    public void openSocket(Session session, EndpointConfig config) throws IOException {
        OT t = new OT();
        try {
            Log.l(TAG, "openSocket", "Called");
            this.wsSession = session;
            this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
            Log.l(TAG, "openSocket", "Opening socket session with id: " + session.getId());
            if (this.httpSession == null || !session.isOpen()) {
                Log.l(TAG, "openSocket", "Not authorized socket request");
                session.getBasicRemote().sendText("{\"type\":\"STATUS\",\"connected\":false,\"data\":null}");
                session.close();
                return;
            }

            Customer customer = FactoryDao.getCustomerDao().getCustomerById(
                    Long.parseLong(this.httpSession.getAttribute(Consts.SESSION_CUSTOMER_KEY).toString()),
                    true);

            try {
                // Close existing sessions for current customer id
                Session oldSession = ConnectMeApp.getChannelSubscription().getSessionByCustomer(customer.getId());
                if (oldSession != null) {
                    oldSession.close();
                }
            } catch (Exception ignore) {
            }

            List<ChannelCustomerRelation> relations = FactoryDao.getChannelCustomerRelationDao().getAllCustomerChannels(customer);

            for (ChannelCustomerRelation relation : relations) {
                ConnectMeApp.getChannelSubscription().subscribeToChannel(relation.getChannelId(), customer, session);
            }
            ConnectMeApp.getChannelSubscription().subscribeToGlobalEvents(customer, session);

            Log.l(TAG, "openSocket", "New socket session opened successfully with id: " + session.getId());
            // Send success connected response to client
            session.getBasicRemote().sendText("{\"type\":\"STATUS\",\"connected\":true,\"data\":null}");
        } catch (Exception e) {
            Log.e(TAG, "openSocket", e.getMessage(), e);
            try {
                session.getBasicRemote().sendText("{\"data\":{\"connected\":false, \"message\":\"500 Server Error\"}}");
            } catch (Exception ignore) {
            }
            session.close();
        } finally {
            Log.l(TAG, "openSocket", "Finished" + t.close());
        }
    }

    @OnMessage
    public void echo(Session session, String msg) throws IOException {
        if (session.isOpen()) {
            //tracks all active sessions
            session.getBasicRemote().sendText("Received message: " + msg);
        }
    }

    @OnClose
    public void socketClosed(Session session) throws IOException {
        Log.l(TAG, "socketClosed", "Closing Socket session with id: " + session.getId());
        try {
            ConnectMeApp.getChannelSubscription().unSubscribeFromAll(session);
        } catch (Exception ignore) {
        }
        try {
            ConnectMeApp.getChannelSubscription().unSubscribeFromGlobalEvents(session);
        } catch (Exception ignore) {
        }
    }
}
