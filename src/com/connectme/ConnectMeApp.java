package com.connectme;

import com.connectme.model.Channel;
import com.connectme.model.Customer;
import com.connectme.sockets.ChannelSubscription;
import com.connectme.util.ConnectionPool;
import com.connectme.util.Consts;
import com.connectme.util.Log;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class ConnectMeApp {

    private static final String TAG = ConnectMeApp.class.getSimpleName();
    public static final boolean DROP_DBS = false;
    private static ConnectMeApp instance;
    private ConnectionPool pool;

    /**
     * {@link Map} contains {@link Channel#id} as key and List of {@link Customer#id} that subscribed to the channel
     */
    private ChannelSubscription channelSubscription;

    public static ConnectMeApp getInstance() throws SQLException {
        return instance == null ? instance = new ConnectMeApp() : instance;
    }

    /**
     * Singleton Application Context, init application DAOs
     * and Database Connection pool
     *
     * @throws SQLException if failed to init the DB Connection Pool
     */
    private ConnectMeApp() throws SQLException {
        Log.lt(TAG, "Constructor", "Init System");
        pool = new ConnectionPool(Consts.DB_DRIVER, Consts.DB_CONNECTION_URL, Consts.DB_USERNAME,
                Consts.DB_PASSWORD, Consts.DB_MAX_CONNECTIONS);
        channelSubscription = new ChannelSubscription();
    }

    public static Connection getDBConnection() throws SQLException {
        Log.l(TAG, "getDBConnection", "Opening DB Connection");
        return getInstance().pool.getConnection();
    }

    public static ChannelSubscription getChannelSubscription() {
        try {
            return getInstance().channelSubscription;
        } catch (Exception e) {
            throw new RuntimeException("Failed to get channelSubscription", e);
        }
    }

    public static void closeDBConnection(Connection connection, Statement statement, ResultSet rs) {
        Log.l(TAG, "closeDBConnection", "Closing DB Connection");
        try {
            if (rs != null)
                rs.close();
        } catch (Exception ignore) {
        }
        try {
            if (statement != null)
                statement.close();
        } catch (Exception ignore) {
        }
        try {
            if (connection != null)
                connection.close();
        } catch (Exception ignore) {
        }

        try {
            ConnectMeApp.getInstance().pool.removeClosedConnections();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void destory() {
        Log.lt(TAG, "destory", "DESTROY SYSTEM");
        try {
            Log.lt(TAG, "destory", "DESTROY SYSTEM DATABASE POOL");
            getInstance().pool.shutdown();
        } catch (Exception ignore) {

        }
        try {
            Log.lt(TAG, "destory", "DESTROY SYSTEM SOCKET SUBSCRIPTIONS");
            getInstance().channelSubscription.shutdown();
        } catch (Exception ignore) {

        }
    }
}
